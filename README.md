# Software : terrain generation studying


I have developped an C++/OpenGL software to test various things on terrain/landscape generation. In this first shot I (dirtily) implemented a method to interpolate the terrain shape given few level curves. The problem: given a set of level curves, we want to be able to generate the terrain height map, or "elevation map" (i.e. a 2D table of altitude values, representing the terrain shape) that should best fit these level curves. 


## Level curves

A level curve is a curve included in a horizontal plan, delimiting the shape of the terrain at this altitude.

![Level curves from the top](Documents/screenshots/level-curves-crop.png)

I allowed my self to have one more information : a boolean value associated ot each level curve, saying it is an extremum or not. Useful to know if we have a top in the space delimited by the curve.

![Level curves altitudes](Documents/screenshots/level-curves-profile.png)


### Principle

_Note : I will name "cell" each elevation value of the heighmap._

![Level curves and corresponding heightmap](Documents/screenshots/heightmap-example2-crop.png)

The principle I first imagined consisted in building along each line of cells of the heightmap, control points being its intersection with the level curves. But this would result in a stairs looking terrain.

My second idea, the one I implemented, consist in 
  - finding all the extremums (obviously situated in the level curves that don't have any upper level curve included in).
  - for each cell, drawing a spline going through the center of the cell and the closest extremum (note : we have a problem if the closest extremum IS also the center of a cell ...).

 ![Terrain and corresponding heightmap](Documents/screenshots/heighmap-example-crop.png)



###  Performance and correctness

Yeah, **this method is horrible**. More to that, **it is incorrect**. The problem is for cells between two tops of the terrain, since we choose the altitude of such cells based on the spline coming out of the closest top. This has the effect of creating gaps in the terrain shape, as you can see in this screenshot :

![Incorrectness of this method](Documents/screenshots/bug.png)

This method is indeed exhaustive (processes a spline for each cell of the heighmap), and thus very slow. I have ideas on how to improve the speed, but I can't think (for the moment ;p) of a way to have a quick method, that is also reliable.

A better solution I imagined while writing these lines, would consist falling back to my first idea (i.e. drawing a spline along each line of the heightmap), but adding _vertical_ level curves (in the XZ and YZ plan), two or more for each extremum of the terrain : one along a line of heightmap, another along a column. I'll try to implement this.


-> I implemented this method. It is indeed faster and avoids the the stair effect described above. This method the thus better.


Horizontal iso-altitudes + added _vertical_ curves:
![Horizontal iso-altitudes + added _vertical_ curves](Documents/screenshots/method2-6.png)

Generated terrain splines based intersecting with (horizontal & vertical) level curves:
![Generated terrain splines based intersecting with (horizontal & vertical) level curves](Documents/screenshots/method2-7.png)

Corresponding terrain :
![Corresponding terrain](Documents/screenshots/method2-8.png) ![Corresponding terrain](Documents/screenshots/method2-9.png)

My problem now, is only a question of spline smoothing.

### From now on ...

Now having spent some hours trying to figure these question by myself, I started searching for some reference on the question. I found somme interesting documents. Namely [this (french) web site](http://www.u-cergy.fr/geosciences/ENSEIGNEMENT/LICENCE/LICENCE-STE/LSTE-S4-geoinfo/CoursGeoinfoL2S4/CoursGeoinfoL2S4_node5.html), and [this (french) paper](https://hal.archives-ouvertes.fr/hal-00308006/document). I am going to take a look at it and try out there methods. To be continued...

![Terrain generator screenshot](Documents/screenshots/presentation.png)

## Procedural terrain generation

Examples of terrains generated with my terrain generator.

![sample01](Documents/screenshots/sample01.jpg)
![sample02](Documents/screenshots/sample02.jpg)
![sample03](Documents/screenshots/sample03.jpg)
![sample04](Documents/screenshots/sample04.jpg)
![sample05](Documents/screenshots/sample05.jpg)
![sample06](Documents/screenshots/sample06.jpg)
![sample07](Documents/screenshots/sample07.jpg)
![sample08](Documents/screenshots/sample08.jpg)
![sample09](Documents/screenshots/sample09.jpg)
![sample10](Documents/screenshots/sample10.jpg)
![sample11](Documents/screenshots/sample11.jpg)
![sample12](Documents/screenshots/sample12.jpg)
![sample13](Documents/screenshots/sample13.jpg)
![sample14](Documents/screenshots/sample14.jpg)
![sample15](Documents/screenshots/sample15.jpg)
![sample16](Documents/screenshots/sample16.jpg)
![sample17](Documents/screenshots/sample17.jpg)
![sample18](Documents/screenshots/sample18.jpg)
![sample19](Documents/screenshots/sample19.jpg)
![sample20](Documents/screenshots/sample20.jpg)
![sample21](Documents/screenshots/sample21.jpg)
![sample22](Documents/screenshots/sample22.jpg)
