/*
 * CProceduralTerrain.h
 *
 *  Created on: May 28, 2010
 *      Author: regis
 */

#ifndef PROCEDURALTERRAIN_H_
#define PROCEDURALTERRAIN_H_

#include "../commonTerrain/heightmap.hpp"
#include "../commonTerrain/terrainmesh.hpp"
#include "../common/drawable.hpp"

using namespace std;

const float fPi = 3.141592;
const double dPi = 3.141592;

class CProceduralTerrain : public CDrawable
{
   private:
   CHeightMap * m_HeightMap;
   CTerrainMesh * m_TerrainMesh;

   bool m_DrawHeightMap;
   bool m_DrawTerrainMesh;
   bool m_DrawSeaMesh;

   // Tool functions
   float sigmoid(float x, float alpha);  // value in [0;1]
   float sign(float x);
   float heaviside(float x);

   float sinNoise(float x, float y, float frequency, float shift, float rotation);
   float base(float x, float y, float frequency, float shift, float rotation);
   
   float peakyBase(float x, float y, float frequency, float waveShift, float waveRotation);
   float roundBase(float x, float y, float frequency, float waveShift, float waveRotation);
   float noise(float t);
   float noise(float min, float max);
   float sinTerrain(float x, float y, float frequency, float shift, float rotation, bool peekOrRound);

   public:
   CProceduralTerrain();
   virtual ~CProceduralTerrain();

   CHeightMap * getHeightMap();
   CTerrainMesh * getTerrainMesh();

   void drawHeightMap(bool);
   void drawTerrainMesh(bool);
   void drawSeaMesh(bool);

   bool drawHeightMap();
   bool drawTerrainMesh();
   bool drawSeaMesh();

   void Build();
   void Draw();

   void BuildHeightMap();
};

#endif /* PROCEDURALTERRAIN_H_ */
