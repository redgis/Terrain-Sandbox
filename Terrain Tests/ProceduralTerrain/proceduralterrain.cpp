/*
 * CTerrain.cpp
 *
 *  Created on: May 28, 2010
 *      Author: regis
 */
#include <GL/glut.h>
#include <math.h>
#include <stdlib.h>
#include "proceduralterrain.hpp"
#include "../common/tools3d.hpp"

 /****************************************************************************/
CProceduralTerrain::CProceduralTerrain() {
   m_HeightMap = new CHeightMap(512, 512/*, -50, 50, -50, 50*/);//256,256);
   m_HeightMap->drawGrid(false);
   m_TerrainMesh = new CTerrainMesh(m_HeightMap);

   m_DrawTerrainMesh = true;
   m_DrawHeightMap = true;
   m_DrawSeaMesh = true;

}

/****************************************************************************/
CProceduralTerrain::~CProceduralTerrain() {
   delete m_TerrainMesh;
   delete m_HeightMap;
}

/****************************************************************************/
CHeightMap * CProceduralTerrain::getHeightMap() {
   return m_HeightMap;
}

/****************************************************************************/
CTerrainMesh * CProceduralTerrain::getTerrainMesh() {
   return m_TerrainMesh;
}

/****************************************************************************/
void CProceduralTerrain::drawHeightMap(bool val) {
   m_DrawHeightMap = val;
}

/****************************************************************************/
void CProceduralTerrain::drawTerrainMesh(bool val) {
   m_DrawTerrainMesh = val;
}

void CProceduralTerrain::drawSeaMesh(bool val) {
   m_DrawSeaMesh = val;
   this->m_TerrainMesh->drawSea(val);
   this->m_TerrainMesh->Build();
}

/****************************************************************************/
bool CProceduralTerrain::drawHeightMap() {
   return m_DrawHeightMap;
}

/****************************************************************************/
bool CProceduralTerrain::drawTerrainMesh() {
   return m_DrawTerrainMesh;
}

bool CProceduralTerrain::drawSeaMesh() {
   return m_DrawSeaMesh;
}

/****************************************************************************/
void CProceduralTerrain::Build() {
   m_HeightMap->Build();
   m_TerrainMesh->Build();

   if (glIsList(m_idDrawList)) {
      glDeleteLists(m_idDrawList, 1);
   }

   //*************************************************************
   /*
   glNewList(m_idDrawList, GL_COMPILE);
   glPushAttrib(GL_ALL_ATTRIB_BITS);
   glPushMatrix();


   glPopMatrix();
   glPopAttrib();
   glEndList();
   */
}

/****************************************************************************/
void CProceduralTerrain::Draw() {

   if (m_DrawTerrainMesh)
      m_TerrainMesh->Draw();

   if (m_DrawHeightMap)
      m_HeightMap->Draw();
}


/** Sigmoid function : smoothed transition from 0 to 1
   *  \param x input value to compute
   *  \param alpha controls the sharpness of the transition. The higher alpha, the sharper the 
   *     transition
   *  \return value toward 0 if x < 0, toward 1 if x > 0
   */
float CProceduralTerrain::sigmoid(float x, float alpha) {
   return 1.0 / (1.0 + expf(-alpha*x));
}

/** Sign function.
*  \param x input value to compute
*  \return -1 if x < 0, 0 if x == 0, 1 if x > 0
*/
float CProceduralTerrain::sign(float x) {
   if (x < 0.0) return -1.0;
   if (x == 0) return 0.0;
   if (x > 0) return 1.0;
}

/** Heaviside function.
   *  \param x input value to compute
   *  \return 0 if x < 0, 1 if x >= 0
   */
float CProceduralTerrain::heaviside(float x) {
   return (x < 0.0) ? 0.0 : 1.0;
}



float CProceduralTerrain::sinNoise(float x, float y, float frequency, float shift, float rotation)
{
   return
      cos(x*frequency + shift) * cos(y*frequency + shift) * cos(rotation);
}

float CProceduralTerrain::base(float x, float y, float frequency, float shift,
   float rotation) 
{
   return cos((cos(rotation) * x + sin(rotation) * y + shift * fPi / 2.0) * frequency);
   //return (sinNoise(x, y, frequency, shift, rotation) + sinNoise(x, y, frequency + 0.1f, shift, rotation) + sinNoise(x, y, frequency + 0.3f, shift, rotation) + sinNoise(x, y, frequency + 0.7f, shift, rotation) + sinNoise(x, y, frequency + 3.0f, shift, rotation)) / 5.0f;
}


/** Base function for perlin noise : peek shaped wave
   * Gnuplot code : base(x, y, frequency, shift, rotation) = 1 - abs( sin( (cos(rotation) * x + 
   *  sin(rotation) * y + shift*pi/2) * frequency ) ) ; splot base(x, y, 1, 0, pi/4)
   *  \param x x position at which to calculate height
   *  \param y y position at which to calculate height
   *  \param frequency number of waves every PI cycle
   *  \param shift wave shift
   *  \param rotation wave orientation
   *  \return height value at (x,y) position
   */
float CProceduralTerrain::peakyBase(float x, float y, float frequency, float shift, 
   float rotation)
{
   return 1.0 - abs( base(x, y, frequency, shift, rotation) );
  
}

/** Base function for perlin noise : round shaped wave
* Gnuplot code : baseRound(x, y, frequency, shift, rotation) = abs( sin( (cos(rotation) * x +
*  sin(rotation) * y + shift*pi/2) * frequency ) ) ; splot baseRound(x, y, 1, 0, pi/4)
*  \param x x position at which to calculate height
*  \param y y position at which to calculate height
*  \param frequency number of waves every PI cycle
*  \param shift wave shift
*  \param rotation wave orientation
*  \return height value at (x,y) position
*/
float CProceduralTerrain::roundBase(float x, float y, float frequency, float shift,
   float rotation)
{
   return abs(base(x, y, frequency, shift, rotation));
}

float CProceduralTerrain::noise(float t) {
   return t * ((float)rand() / (float)RAND_MAX);
}

float CProceduralTerrain::noise(float min, float max) {
   return min + noise(max - min);
}

/****************************************************************************/
float CProceduralTerrain::sinTerrain(float x, float y, float frequency,
   float shift, float rotation, bool peekOrRound)
{
   float result = 0;

      
   if (peekOrRound) {
      result = peakyBase(x, y, frequency, shift, rotation) * peakyBase(x, y, frequency, shift, rotation + fPi / 6.0f);
   } else {
      result = roundBase(x, y, frequency, shift, rotation) * roundBase(x, y, frequency, shift, rotation + fPi /   6.0f);
   }

   return result;
}

/****************************************************************************/
void CProceduralTerrain::BuildHeightMap() {
   int ySize = m_HeightMap->getYSize();
   int xSize = m_HeightMap->getXSize();
   float xMin = m_HeightMap->getXMin();
   float yMin = m_HeightMap->getYMin();
   float xMax = m_HeightMap->getXMax();
   float yMax = m_HeightMap->getYMax();
   float xOff = m_HeightMap->getXOffset();
   float yOff = m_HeightMap->getYOffset();

   // Initialize heightmap
   for (int xIndex = 0; xIndex < xSize; xIndex++) {
      for (int yIndex = 0; yIndex < ySize; yIndex++) {
         (*m_HeightMap)[xIndex][yIndex] = 0;
      }
   }

   // octave : the higher the more detailed
   int nbOctaves = 20; // Number of octaves
   float amplitude = 2.56; // Amplitude of this octave
   float frequency = 0.1f; // Frequency decides the granularity of this octave
   for (int octave = 0; octave < nbOctaves; octave++) {
      amplitude /= 1.4f;   // Amplitude decrease at each octave
      frequency *= 1.3f;   // Refine granularity at each octave
      float shift = noise(fPi);  // Add a randome shift to the base forme
      float rotation = noise(2.0f * fPi); // Add a horizontal rotation to the base form

      bool typeOfBase = noise(1.99f); // Decides whether use peaky or round shaped form for this octave.

      for (int xIndex = 0; xIndex < xSize; xIndex++) {
         for (int yIndex = 0; yIndex < ySize; yIndex++) {

            (*m_HeightMap)[xIndex][yIndex] +=
               amplitude * (sinTerrain(
                  xMin + xIndex*xOff,
                  yMin + yIndex*yOff,
                  frequency,
                  shift,
                  rotation,
                  typeOfBase) - 0.17f); // -0.3f to have a flat ground (smoothed by sigmoid below)
         }
      }
   }

   // Amplify the peaks by applying using an exponential. This will either make the peaks sharper,
   // or the rounder. See http://www.redblobgames.com/maps/terrain-from-noise/
   float exponent = noise(0.2f, 3.0f);   
   for (int xIndex = 0; xIndex < xSize; xIndex++) {
      for (int yIndex = 0; yIndex < ySize; yIndex++) {
         (*m_HeightMap)[xIndex][yIndex] =
            (*m_HeightMap)[xIndex][yIndex] > 0 ?
               pow((*m_HeightMap)[xIndex][yIndex], exponent) :
               (*m_HeightMap)[xIndex][yIndex];
      }
   }

   if (!drawSeaMesh()) {
      // if we are not drawing the sea, level the prairies
      for (int xIndex = 0; xIndex < xSize; xIndex++) {
         for (int yIndex = 0; yIndex < ySize; yIndex++) {
            (*m_HeightMap)[xIndex][yIndex] *= sigmoid((*m_HeightMap)[xIndex][yIndex], 5.0f);
         }
      }
   }

}

