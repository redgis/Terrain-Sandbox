/*
 * terracurv.h
 *
 *  Created on: May 26, 2010
 *      Author: Régis Martinez - regis.martinez3@free.fr
 */

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <GL/glui.h>
#include "main.hpp"
#include "../common/tools3d.hpp"

using namespace std;




/****************************************************************************/
void InitGL(int Width, int Height)              // We call this right after our OpenGL window is created.
{
   glClearColor(0.0f, 0.3f, 0.4f, 0.0f);         // This Will Clear The Background Color To Black
   glClearDepth(1.0);                            // Enables Clearing Of The Depth Buffer
   //glDepthFunc(GL_LESS);                         // The Type Of Depth Test To Do
   glEnable(GL_DEPTH_TEST);                      // Enables Depth Testing
   glShadeModel(GL_SMOOTH);                      // Enables Smooth Color Shading

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();                             // Reset The Projection Matrix

   gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);     // Calculate The Aspect Ratio Of The Window

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   //Lights
   glEnable(GL_LIGHT0);
   glLightfv(GL_LIGHT0, GL_AMBIENT, light0_ambient);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, light0_diffuse);
   glLightfv(GL_LIGHT0, GL_POSITION, light0_position);

   glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_FALSE);

   glPointSize(5.0);
}

/****************************************************************************/
void ReSizeGLScene(int Width, int Height)
{
   if (Height == 0)                                // Prevent A Divide By Zero If The Window Is Too Small
      Height = 1;

   glViewport(0, 0, Width, Height);              // Reset The Current Viewport And Perspective Transformation

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();

   gluPerspective(45.0f, (GLfloat)Width / (GLfloat)Height, 0.1f, 100.0f);
   glMatrixMode(GL_MODELVIEW);
}

/****************************************************************************/
void DrawGLScene()
{
   /* Clear background */
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluOrtho2D(0.0, glutGet(GLUT_WINDOW_WIDTH), 0.0, glutGet(GLUT_WINDOW_HEIGHT));
   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();

   glEnable(GL_LIGHTING);

   //My drawing here
   myCamera->ApplyProjection();

   glMaterialfv(GL_FRONT, GL_AMBIENT, material0_ambient);
   glMaterialfv(GL_FRONT, GL_SPECULAR, material0_specular);
   glMaterialf(GL_FRONT, GL_SHININESS, 100.0);


   //draw axis
   glDisable(GL_LIGHTING);
   glBegin(GL_LINES);
   glColor3f(0.0, 0.0, 1.0);
   glVertex3f(0.0, 0.0, 0.0);
   glVertex3f(1.0, 0.0, 0.0);

   glColor3f(1.0, 0.0, 0.0);
   glVertex3f(0.0, 0.0, 0.0);
   glVertex3f(0.0, 1.0, 0.0);

   glColor3f(0.0, 1.0, 0.0);
   glVertex3f(0.0, 0.0, 0.0);
   glVertex3f(0.0, 0.0, 1.0);
   glEnd();
   glEnable(GL_LIGHTING);

   myTerrain->Draw();

   glEnable(GL_LIGHTING);

   glPushMatrix();
   glMultMatrixf(lights_rotation);
   glLightfv(GL_LIGHT0, GL_POSITION, light0_position);

   glPointSize(10.0);
   glDisable(GL_LIGHTING);
   glBegin(GL_POINTS);
   glColor3f(1.0, 1.0, 0.0);
   glVertex3fv(light0_position);
   glEnable(GL_LIGHTING);
   glEnd();
   glPointSize(5.0);
   glPopMatrix();


   // swap the buffers to display, since double buffering is used.
   glutSwapBuffers();
}

/*****************************************************************************/
void Mouse(int button, int state, int x, int y)
{
   /* Si on appuie sur le bouton gauche */
   if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
      Presse = 1; /* Le booleen presse passe a 1 (vrai) */
      Xold = x;   /* On sauvegarde la position de la souris */
      Yold = y;
   }
   /* Si on relache le bouton gauche */
   if (button == GLUT_LEFT_BUTTON && state == GLUT_UP)
      Presse = 0; /* Le booleen presse passe a 0 (faux) */
}

/*****************************************************************************/
void MouseMove(int x, int y)
{
   //cout << "mouvX: " << Xold-x << " largeur: " << glutGet(GLUT_WINDOW_WIDTH) << " Xold: " << Xold << " x: " << x << endl;
   //cout << "mouvY: " << Yold-y << " hauteur: " << glutGet(GLUT_WINDOW_HEIGHT) << " Yold: " << Yold << " y: " << y << endl << endl;

   if (Presse) {
      /* serie de test corrigeant le bug d'affichage du au retard du Pointer Warp */
      if ((Xold - x) > glutGet(GLUT_WINDOW_WIDTH) - 3)
         myCamera->YawLeftRight(Xold - x - glutGet(GLUT_WINDOW_WIDTH));
      else if ((Xold - x) < -(glutGet(GLUT_WINDOW_WIDTH) - 3))
         myCamera->YawLeftRight(Xold - x + glutGet(GLUT_WINDOW_WIDTH));
      else
         myCamera->YawLeftRight(Xold - x);

      /* serie de tests corrigeant le bug d'affichage du au retard du Pointer Warp */
      if ((Yold - y) > glutGet(GLUT_WINDOW_HEIGHT) - 3)
         myCamera->Pitch(Yold - y - glutGet(GLUT_WINDOW_HEIGHT));
      else if ((Yold - y) < -(glutGet(GLUT_WINDOW_HEIGHT) - 3))
         myCamera->Pitch(Yold - y + glutGet(GLUT_WINDOW_HEIGHT));
      else
         myCamera->Pitch(Yold - y);

      glutPostRedisplay();

      if (x >= glutGet(GLUT_WINDOW_WIDTH) - 1) {
         x = 1;
         glutWarpPointer(x, y);
      } else if (x <= 0) {
         x = glutGet(GLUT_WINDOW_WIDTH) - 2;
         glutWarpPointer(x, y);
      }

      if (y >= glutGet(GLUT_WINDOW_HEIGHT) - 1) {
         y = 1;
         glutWarpPointer(x, y);
      } else if (y <= 0) {
         y = glutGet(GLUT_WINDOW_HEIGHT) - 2;
         glutWarpPointer(x, y);
      }

      Xold = x;
      Yold = y;
   }
}

/*****************************************************************************/
void Keyboard(unsigned char keyPressed, int x, int y)
{
   switch (keyPressed) {
      case 'z':
         myCamera->MoveAhead(MoveStep);
         glutPostRedisplay();
         break;

      case 's':
         myCamera->MoveBack(MoveStep);
         glutPostRedisplay();
         break;

      case ' ':
         myCamera->MoveUp(MoveStep);
         glutPostRedisplay();
         break;

      case 'a':
         myCamera->MoveDown(MoveStep);
         glutPostRedisplay();
         break;

      case 'q':
         myCamera->MoveLeft(MoveStep);
         glutPostRedisplay();
         break;

      case 'd':
         myCamera->MoveRight(MoveStep);
         glutPostRedisplay();
         break;

      case 27:
         glutDestroyWindow(terraMainWindow);
         exit(0);
         break;
   }
}

/*****************************************************************************/
void SpecialKeys(int keyPressed, int x, int y)
{
   switch (keyPressed) {
      case GLUT_KEY_F1:
         break;

      case GLUT_KEY_F2:
         break;

      case GLUT_KEY_F3:
         break;

      case GLUT_KEY_F4:
         break;

      case GLUT_KEY_F5:
         break;

      case GLUT_KEY_F6:
         break;

      case GLUT_KEY_F9: /* FullScreen */
         break;

      case GLUT_KEY_F10: /* affichage filaire / polygonal */
         break;

      case GLUT_KEY_F11: /* affichage lisse/facettes */
         break;

      case GLUT_KEY_F12:
         break;

      case GLUT_KEY_RIGHT:
         break;

      case GLUT_KEY_LEFT:
         break;

      case GLUT_KEY_UP:
         break;

      case GLUT_KEY_DOWN:
         break;

      case GLUT_KEY_HOME:
         break;

      case GLUT_KEY_END:
         break;

      case GLUT_KEY_PAGE_UP:
         break;

      case GLUT_KEY_PAGE_DOWN:
         break;
   }
}

/****************************************************************************/
void gluiControl_Callback(int control)
{
   switch (control) {
      case GUI_FULLSCREEN:
         break;

      case GUI_WIREFRAME:
         if (doWireFrame) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            myTerrain->Build();
         } else {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            myTerrain->Build();
         }
         break;

      case GUI_TERRAIN_SIZE:
         myTerrain->getHeightMap()->setSize(myTerrainSize, myTerrainSize);
         break;

      case GUI_REDBUILD_TERRAIN:
         srand(myRandomSeed);
         myTerrain->BuildHeightMap();
         myTerrain->Build();
         glutPostRedisplay();
         break;

      case GUI_RANDOMIZE_AND_REDBUILD_TERRAIN:
         srand(time(NULL));
         myRandomSeed = rand();
         myGui->sync_live();
         srand(myRandomSeed);
         myTerrain->BuildHeightMap();
         myTerrain->Build();
         glutPostRedisplay();
         break;

      case GUI_SHOW_NORMALS:
         myTerrain->getTerrainMesh()->drawNormals(showNormals);
         myTerrain->Build();
         glutPostRedisplay();
         break;

      case GUI_SHOW_HEIGHTMAP:
         myTerrain->drawHeightMap(showHeightMap);
         glutPostRedisplay();
         break;

      case GUI_SMOOTH_OR_FLAT:
         if (doSmooth)
            glShadeModel(GL_SMOOTH);
         else
            glShadeModel(GL_FLAT);
         glutPostRedisplay();
         break;

      case GUI_USE_TRIANGLES:
         myTerrain->getTerrainMesh()->useTriangles(useTriangles);
         myTerrain->Build();
         glutPostRedisplay();
         break;

      case GUI_SHOW_TERRAIN:
         myTerrain->drawTerrainMesh(showTerrain);
         glutPostRedisplay();
         break;

      case GUI_SHOW_SEA:
         myTerrain->drawSeaMesh(showSea);
         srand(myRandomSeed);
         myTerrain->BuildHeightMap();
         myTerrain->Build();
         glutPostRedisplay();
         break;

      case GUI_RANDOM_SEED:
         break;

      default:
         break;
   }
}

/****************************************************************************/
void guiBuild()
{
   myGui = GLUI_Master.create_glui_subwindow(terraMainWindow, GLUI_SUBWINDOW_RIGHT);

   // Fullscreen button
   new GLUI_Button(myGui, "Full screen", 0, (GLUI_Update_CB)glutFullScreen);

   // Quit button
   new GLUI_Button(myGui, "Quit", 0, (GLUI_Update_CB)exit);

   // Light rotation spinner
   GLUI_Rotation * LightRotation = new GLUI_Rotation(myGui, "Light", lights_rotation);
   LightRotation->set_spin(1.0);

   // Wireframe checkbox
   new GLUI_Checkbox(myGui, "Wireframe", &doWireFrame, GUI_WIREFRAME, (GLUI_CB)gluiControl_Callback);

   // Show terrain checkbox
   new GLUI_Checkbox(myGui, "Show terrain", &showTerrain, GUI_SHOW_TERRAIN, (GLUI_CB)gluiControl_Callback);

   // Show sea checkbox
   new GLUI_Checkbox(myGui, "Show sea", &showSea, GUI_SHOW_SEA, (GLUI_CB)gluiControl_Callback);

   // Smooth / flat checkbox
   new GLUI_Checkbox(myGui, "Smooth", &doSmooth, GUI_SMOOTH_OR_FLAT, (GLUI_CB)gluiControl_Callback);

   
   // Triangles / quads checkbox
   new GLUI_Checkbox(myGui, "Triangles (versus quads)", &useTriangles, GUI_USE_TRIANGLES, (GLUI_CB)gluiControl_Callback);

   // Show normals checkbox
   new GLUI_Checkbox(myGui, "Show normals", &showNormals, GUI_SHOW_NORMALS, (GLUI_CB)gluiControl_Callback);

   // Show heightmap checkbox
   new GLUI_Checkbox(myGui, "Show heightmap", &showHeightMap, GUI_SHOW_HEIGHTMAP, (GLUI_CB)gluiControl_Callback);

   // Terrain size spinner
   GLUI_Spinner * TerrainSizeSpinner = new GLUI_Spinner(myGui, "tiles:", &myTerrainSize, GUI_TERRAIN_SIZE, (GLUI_CB)gluiControl_Callback);
   TerrainSizeSpinner->set_int_limits(10, 1000);

   // Random seed
   GLUI_Spinner * RandomSeed = new GLUI_Spinner(myGui, "seed:", &myRandomSeed, GUI_RANDOM_SEED, (GLUI_CB)gluiControl_Callback);
   RandomSeed->set_int_limits(0, RAND_MAX);

   // Terrain rebuild button
   new GLUI_Button(myGui, "Rebuild", GUI_REDBUILD_TERRAIN, (GLUI_CB)gluiControl_Callback);

   new GLUI_Button(myGui, "Randomize & Rebuild", GUI_RANDOMIZE_AND_REDBUILD_TERRAIN, (GLUI_CB)gluiControl_Callback);

   new GLUI_StaticText(myGui, "Click and wait");




   /**** Link windows to GLUI, and register idle callback ******/
   myGui->set_main_gfx_window(terraMainWindow);

}

/****************************************************************************/
int main(int argc, char * argv[])
{
   /* Initialize GLUT state - glut will take any command line arguments that pertain to it or
      X Windows - look at its documentation at http://reality.sgi.com/mjk/spec3/spec3.html */
   glutInit(&argc, argv);

   /* Select type of Display mode:
      Double buffer
      RGBA color
      Alpha components supported
      Depth buffered for automatic clipping */
   glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH);

   /* get a 640 x 480 window */
   glutInitWindowSize(1024, 768);

   /* the window starts at the upper left corner of the screen */
   glutInitWindowPosition(0, 0);

   /* Open a window */
   terraMainWindow = glutCreateWindow("Terra Curv - Regis Martinez");

   /* Register the function called when the keyboard is pressed. */
   glutDisplayFunc(&DrawGLScene);
   glutMotionFunc(&MouseMove);
   GLUI_Master.set_glutReshapeFunc(&ReSizeGLScene);
   GLUI_Master.set_glutKeyboardFunc(&Keyboard);
   GLUI_Master.set_glutSpecialFunc(&SpecialKeys);
   GLUI_Master.set_glutMouseFunc(&Mouse);
   //GLUI_Master.set_glutIdleFunc(&DrawGLScene);

   /* Initialize our window. */
   InitGL(1024, 768);

   MoveStep = 0.05;
   Xold = 0;
   Yold = 0;
   Presse = '\0';

   myTerrain = new CProceduralTerrain();

   myTerrain->BuildHeightMap();
   myTerrain->Build();
   
   myCamera = new CCamera(-10.0f + 0.5f, 1.5f, -10.0f + 0.5f, 140.0f, -5.0f);
   myCamera->InitCamera((double)90.0, (double)4 / (double)3, (double) 0.01, (double)1000);

   guiBuild();


   /* Start Event Processing Engine */
   glutMainLoop();

}

