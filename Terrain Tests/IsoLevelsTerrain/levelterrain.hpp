/*
 * CTerrain.h
 *
 *  Created on: May 28, 2010
 *      Author: regis
 */

#ifndef LEVELTERRAIN_H_
#define LEVELTERRAIN_H_

#include <deque>
#include <map>
#include "levelcurve.hpp"
#include "../commonTerrain/heightmap.hpp"
#include "../commonTerrain/terrainmesh.hpp"
#include "../common/drawable.hpp"

using namespace std;

class CLevelTerrain : public CDrawable
{
  private:
    deque<CSpline *> m_BuildingSplines;
    deque<CPoint *> m_ContactPoints;

    deque <CPoint *> m_Extremums;
    deque <CLevelCurve *> m_LevelCurves;
    deque <CLevelCurve *> m_Couples;
    CHeightMap * m_HeightMap;
    CTerrainMesh * m_TerrainMesh;

    bool m_DrawHeightMap;
    bool m_DrawLevelCurves;
    bool m_DrawTerrainMesh;
    bool m_DrawBuildingSplines;
    float m_TerrainSmoothness;

    void CalculateHeightMapBounds ();
    void CalculateExtremums ();

    CSpline * castSpline (CPoint Point1, CPoint Point2);
    CLevelCurve * castNewLevelCurve (CPoint Point1, CPoint Point2, CPoint * emittingExtremum);
    void recastCouple (CLevelCurve *);


  public:
    CLevelTerrain ();
    virtual ~CLevelTerrain();

    void AddLevelCurve (CLevelCurve *);

    void drawHeightMap (bool);
    void drawLevelCurves (bool);
    void drawTerrainMesh (bool);
    void drawBuildingSplines (bool);

    bool drawHeightMap ();
    bool drawLevelCurves ();
    bool drawTerrainMesh ();
    bool drawBuildingSplines ();


    void BuildHeightMap_Brute ();
    void BuildHeightMap_Couples ();
    CPoint * getClosestExtremum (CPoint & pt);
    CHeightMap * getHeightMap ();
    CTerrainMesh * getTerrainMesh ();

    void Build ();
    void Draw ();
};

#endif /* LEVELTERRAIN_H_ */
