/*
 * levelcurve.cpp
 *
 *  Created on: May 27, 2010
 *      Author: Regis Martinez - regis.martinez3@free.fr
 */

#include "levelcurve.hpp"
#include "levelterrain.hpp"
#include "../common/tools3d.hpp"

/*****************************************************************************/
CLevelCurve::CLevelCurve(int Discretization, float Height, bool extremum, CPoint * emitExtremum) : CSpline(Discretization), m_Height(Height), m_IsExtremum(extremum), m_EmittingExtremum (emitExtremum)
{
  setCatmullRomMatrix (0.7);
  //setHermiteMatrix ();
  //setBSplineMatrix();
  //setBezierMatrix();
}

/*****************************************************************************/
CLevelCurve::~CLevelCurve()
{

}

/****************************************************************************/
void CLevelCurve::addPoint (CPoint * newPoint)
{
  newPoint->SetY(m_Height);
  CSpline::addPoint (newPoint);
}

/****************************************************************************/
void CLevelCurve::addPoint (CPoint & newPoint)
{
  newPoint.SetY(m_Height);
  CSpline::addPoint (newPoint);
}

/****************************************************************************/
void CLevelCurve::addPoint (float x, float z)
{
  CSpline::addPoint(x, m_Height, z);
}

/****************************************************************************/
bool CLevelCurve::IsExtremum ()
{
  return m_IsExtremum;
}

/****************************************************************************/
CPoint * CLevelCurve::getCenter ()
{
  CPoint * result = new CPoint (0.0, 0.0, 0.0, 0.0);
  CPoint tmp = CPoint ();
  float t = 0.0;


  for (int Index = 0; Index <= m_Discretization-1; Index++)
  {
    t = (float)Index/(float)(m_Discretization-1);
    tmp = getSplinePoint (t);//CPoint (1.0, 1.0, 1.0, 1.0);
    (*result) += tmp;
    //cout << "SplinePoint: " << (CPoint) getSplinePoint (t) << " | result -  " << (*tmp) << endl;
  }

  (*result) /= (float)m_Discretization;
  //cout << "Final result : " << *(tmp) << endl;
  return result;
}

/****************************************************************************/
CPoint * CLevelCurve::getEmittingExtremum ()
{
  return m_EmittingExtremum;
}

/****************************************************************************/
float CLevelCurve::getHeight ()
{
  return m_Height;
}

/****************************************************************************/
void CLevelCurve::Build ()
{
  CSpline::Build();
}

/****************************************************************************/
void CLevelCurve::Draw ()
{
  CSpline::Draw ();
}

/****************************************************************************/
int CLevelCurve::getFiltered2DIntersections (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, CLevelTerrain * levelTerrain, int Axis0, int Axis1, int Axis2, bool lineIntersection)
{
  CPoint Point1;
  CPoint Point2;
  CPoint * tmpIntersection;
  CPoint resultIntersection;

  CPoint tmpPoint1;
  CPoint tmpPoint2;
  CPoint tmpA;
  CPoint tmpB;

  int N = getNbPoints ();
  int NbIntersect = 0;

  deque<CPoint *>::iterator itIntersection;
  deque<CPoint *>::iterator itLastIntersection;

  if (N >= 4)
  {
    Point1 = (CPoint) getSplinePoint (0.0);
    for (int Index = 1; Index <= m_Discretization-1; Index++)
    {
      float t = (float)Index/(float)(m_Discretization-1);
      Point2 = (CPoint) getSplinePoint (t);

      // Convert points to make intersection in XY plan
      tmpPoint1[0] = Point1[Axis0];
      tmpPoint1[1] = Point1[Axis1];
      tmpPoint1[2] = Point1[Axis2];

      tmpPoint2[0] = Point2[Axis0];
      tmpPoint2[1] = Point2[Axis1];
      tmpPoint2[2] = Point2[Axis2];

      tmpA[0] = A[Axis0];
      tmpA[1] = A[Axis1];
      tmpA[2] = A[Axis2];

      tmpB[0] = B[Axis0];
      tmpB[1] = B[Axis1];
      tmpB[2] = B[Axis2];

      // Calculate intersection
      if (lineIntersection)  // Should (AB) be considered as a line or as a segment ?
        tmpIntersection = Intersect2DLineToSegment (tmpA, tmpB, tmpPoint1, tmpPoint2);
      else
        tmpIntersection = Intersect2DSegments (tmpA, tmpB, tmpPoint1, tmpPoint2);
      if (tmpIntersection)
      {
        // Reconvert coordinates of intersaction
        resultIntersection = *tmpIntersection;
        (*tmpIntersection)[Axis0] = resultIntersection[0];
        (*tmpIntersection)[Axis1] = resultIntersection[1];
        (*tmpIntersection)[Axis2] = resultIntersection[2];

        if (m_EmittingExtremum == levelTerrain->getClosestExtremum(*tmpIntersection))
        {
          itIntersection = Intersections.begin ();
          itLastIntersection = Intersections.end ();
          while ((itIntersection != itLastIntersection) && (*((*itIntersection)) * A < (*tmpIntersection) * A) )
          {
            itIntersection ++;
          }
          Intersections.insert(itIntersection, tmpIntersection);
        }

        NbIntersect++;
      }

      Point1 = Point2;
    }
  }

  return NbIntersect;
}
