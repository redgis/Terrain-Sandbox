/*
 * CTerrain.cpp
 *
 *  Created on: May 28, 2010
 *      Author: regis
 */
#include <GL/glut.h>
#include <math.h>
#include "levelterrain.hpp"
#include "../common/tools3d.hpp"
#include "../common/stltools.hpp"

/****************************************************************************/
CLevelTerrain::CLevelTerrain()
{
  m_LevelCurves.clear ();
  m_HeightMap = new CHeightMap (50, 50);//256,256);
  m_HeightMap->drawGrid(false);
  m_TerrainMesh = new CTerrainMesh (m_HeightMap);

  m_DrawLevelCurves = false;
  m_DrawTerrainMesh = true;
  m_DrawHeightMap = true;
  m_DrawBuildingSplines = false;
}

/****************************************************************************/
CLevelTerrain::~CLevelTerrain()
{
  delete m_TerrainMesh;
  delete m_HeightMap;

  //clean member stuff (level curves, extremums, contact points, building splines)

  // m_BuildingSplines;
  cleanStuffs(m_BuildingSplines);

  //m_ContactPoints m_Extremums;
  cleanStuffs (m_ContactPoints);
  cleanStuffs (m_Extremums);

  //m_LevelCurves m_Couples;
  cleanStuffs (m_LevelCurves);
  cleanStuffs (m_Couples);
}

/****************************************************************************/
void CLevelTerrain::AddLevelCurve (CLevelCurve * levelCurve)
{
  m_LevelCurves.push_back(levelCurve);

  //cout << "size :" << m_LevelCurves.size() << endl;
}

/****************************************************************************/
CHeightMap * CLevelTerrain::getHeightMap ()
{
  return m_HeightMap;
}

/****************************************************************************/
CTerrainMesh * CLevelTerrain::getTerrainMesh ()
{
  return m_TerrainMesh;
}

/****************************************************************************/
void CLevelTerrain::Build ()
{
  m_HeightMap->Build ();
  m_TerrainMesh->Build ();

  if (glIsList (m_idDrawList))
    glDeleteLists (m_idDrawList, 1);

  //*****************************************************************
  // be sure the level curves are built !

  //Build level curves splines
  deque <CLevelCurve *>::iterator currentCurve  = m_LevelCurves.begin();
  deque <CLevelCurve *>::iterator lastCurve  = m_LevelCurves.end();
  while (currentCurve != lastCurve)
  {
    (*currentCurve)->Build();
    currentCurve++;
  }

  //Build couples splines
  currentCurve  = m_Couples.begin();
  lastCurve  = m_Couples.end();
  while (currentCurve != lastCurve)
  {
    (*currentCurve)->Build();
    currentCurve++;
  }


  //*************************************************************
  glNewList(m_idDrawList, GL_COMPILE);
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glPushMatrix();

  // Draw the building splines (useful for debug)
  if (m_DrawBuildingSplines)
  {

    glDisable (GL_LIGHTING);
    deque<CPoint *>::iterator itContact = m_ContactPoints.begin();
    deque<CPoint *>::iterator itLastContact = m_ContactPoints.end();

    /** /
    glColor3f(0.0, 0.0, 1.0);
    glBegin (GL_LINES);
    itContact = m_ContactPoints.begin();
    itLastContact = m_ContactPoints.end();
    while (itContact != itLastContact)
    {
      (*itContact)->gl();
      glVertex3f(0.0, 0.0, 0.0);
      itContact++;
    }
    glEnd();
    /**/

    glColor3f(1.0, 0.0, 0.0);
    glBegin (GL_POINTS);
    itContact = m_ContactPoints.begin();
    itLastContact = m_ContactPoints.end();
    while (itContact != itLastContact)
    {
      (*itContact)->gl();
      itContact++;
    }
    glEnd();
    /**/

    deque<CSpline *>::iterator itSpline = m_BuildingSplines.begin();
    deque<CSpline *>::iterator itLastSpline = m_BuildingSplines.end();
    while (itSpline != itLastSpline)
    {
      (*itSpline)->Draw ();
      itSpline++;
    }

  }


  int polygonMode[2];
  glGetIntegerv(GL_POLYGON_MODE, polygonMode);

  if (m_DrawLevelCurves)
  {
    if (polygonMode[0] == GL_LINE)
    {
      glDisable (GL_DEPTH_TEST);
      glDepthMask (GL_FALSE);
    }

    // Execute the level curves CallList
    currentCurve  = m_LevelCurves.begin();
    lastCurve  = m_LevelCurves.end();
    while (currentCurve != lastCurve)
    {
      (*currentCurve)->Draw();
      currentCurve++;
    }

    // Execute the couples CallList
    currentCurve  = m_Couples.begin();
    lastCurve  = m_Couples.end();
    while (currentCurve != lastCurve)
    {
      (*currentCurve)->Draw();
      currentCurve++;
    }

    // Draw extremums
    glDisable(GL_LIGHTING);
    glBegin (GL_POINTS);
    deque <CPoint *>::iterator currentExtremum = m_Extremums.begin();
    deque <CPoint *>::iterator lastExtremum = m_Extremums.end();
    while (currentExtremum != lastExtremum)
    {
      glColor3f (0.0, 0.0, 1.0);
      (*currentExtremum)->gl();
      currentExtremum ++;
    }
    glEnd();
    glEnable(GL_LIGHTING);

    if(polygonMode[0] == GL_LINE)
    {
      glDepthMask (GL_TRUE);
      glEnable(GL_DEPTH_TEST);
    }
  }

  glPopMatrix();
  glPopAttrib();
  glEndList();
}

/****************************************************************************/
void CLevelTerrain::Draw ()
{
  if (m_DrawTerrainMesh)
    m_TerrainMesh->Draw ();

  if (m_DrawHeightMap)
    m_HeightMap->Draw ();

  CDrawable::Draw ();
}

/****************************************************************************/
void CLevelTerrain::drawHeightMap (bool val)
{
  m_DrawHeightMap = val;
}

/****************************************************************************/
void CLevelTerrain::drawLevelCurves (bool val)
{
  m_DrawLevelCurves = val;
}

/****************************************************************************/
void CLevelTerrain::drawTerrainMesh (bool val)
{
  m_DrawTerrainMesh = val;
}

/****************************************************************************/
void CLevelTerrain::drawBuildingSplines (bool val)
{
  m_DrawBuildingSplines = val;
}

/****************************************************************************/
bool CLevelTerrain::drawHeightMap ()
{
  return m_DrawHeightMap;
}

/****************************************************************************/
bool CLevelTerrain::drawLevelCurves ()
{
  return m_DrawLevelCurves;
}

/****************************************************************************/
bool CLevelTerrain::drawTerrainMesh ()
{
  return m_DrawTerrainMesh;
}

/****************************************************************************/
bool CLevelTerrain::drawBuildingSplines ()
{
  return m_DrawBuildingSplines;
}

/****************************************************************************/
CPoint * CLevelTerrain::getClosestExtremum (CPoint & pt)
{
  CPoint * result = NULL;
  float shortestDistance = -1;
  float currentDistance;

  float ptY = pt.Y();

  deque<CPoint *>::iterator itCurrentPoint = m_Extremums.begin();
  deque<CPoint *>::iterator itLastPoint = m_Extremums.end();

  while (itCurrentPoint != itLastPoint)
  {
    pt.SetY((*itCurrentPoint)->Y());
    currentDistance = DistancePointToPoint (*(*itCurrentPoint), pt);
    if (currentDistance < shortestDistance || shortestDistance == -1)
    {
      shortestDistance = currentDistance;
      result = (*itCurrentPoint);
    }
    pt.SetY(ptY);
    itCurrentPoint ++;
  }

  return result;
}

/****************************************************************************/
void CLevelTerrain::BuildHeightMap_Brute ()
{

  //clean member stuff (points, building splines)
  cleanStuffs(m_ContactPoints);
  cleanStuffs(m_Extremums);
  cleanStuffs(m_BuildingSplines);

  //*******************************************************************
  // Calculate extremums
  CalculateExtremums ();

  //*******************************************************************
  // Set Heightmap bounds
  CalculateHeightMapBounds ();

  //*******************************************************************
  // Calculate height of each heigthmap componant
  CPoint * closestExtremum;
  CPoint hmPoint;
  CPoint hmPointVertical;

  CSpline * tmpSpline;
  deque<CPoint *> pointList;

  float xOffset = m_HeightMap->getXOffset ();
  float yOffset = m_HeightMap->getYOffset ();

  deque<CPoint *>::iterator itExtremum = m_Extremums.begin();
  deque<CPoint *>::iterator itLastExtremum = m_Extremums.end();

  m_BuildingSplines.clear ();
  m_ContactPoints.clear ();

  while (itExtremum != itLastExtremum)
  {
    //closestExtremum = (*itExtremum);

    for (int xIndex = 0; xIndex < m_HeightMap->getXSize(); xIndex++)
    {
      for (int yIndex = 0; yIndex < m_HeightMap->getYSize(); yIndex++)
      {
        //Center of current heightmap cell
        hmPoint = CPoint(m_HeightMap->getXMin() + xIndex * xOffset + xOffset/2.0, 0.0, m_HeightMap->getYMin() + yIndex * yOffset + yOffset/2.0);
        closestExtremum = getClosestExtremum (hmPoint);

        //Build spline
        pointList.clear();
        deque<CLevelCurve *>::iterator itCurrentCurve = m_LevelCurves.begin();
        deque<CLevelCurve *>::iterator itLastCurve = m_LevelCurves.end();

        while (itCurrentCurve != itLastCurve)
        {
          (*itCurrentCurve)->get2DIntersections (hmPoint, *closestExtremum, pointList, 0, 2, 1);
          itCurrentCurve++;
        }

        if (pointList.size()>=4)
        {
          tmpSpline = new CSpline (m_HeightMap->getXSize()+10);
          tmpSpline->addControlPoints (pointList);
          tmpSpline->reachAllPoints(true);
          tmpSpline->Build();
          m_BuildingSplines.push_back(tmpSpline);

          pointList.clear();
          hmPointVertical = hmPoint + CPoint (0.0, 1.0, 0.0);
          tmpSpline->get2DIntersections (hmPoint, hmPointVertical, pointList);
          if ((*m_HeightMap)[xIndex][yIndex] <= pointList[0]->Y())
            (*m_HeightMap)[xIndex][yIndex] = pointList[0]->Y();

          m_ContactPoints.insert(m_ContactPoints.end(), pointList.begin(), pointList.end());
        }

      }
    }
    break;
    itExtremum ++;
  }
}

/****************************************************************************/
void CLevelTerrain::BuildHeightMap_Couples ()
{
  //clean member stuff (points, building splines)
  cleanStuffs(m_ContactPoints);
  cleanStuffs(m_Extremums);
  cleanStuffs(m_BuildingSplines);
  cleanStuffs (m_Couples);


  //*******************************************************************
  // Calculate extremums
  CalculateExtremums ();


  //*******************************************************************
  // Set Heightmap bounds
  CalculateHeightMapBounds ();


  //*******************************************************************
  // find  x and y extremums of each level curve
  /** /
  CPoint xMinPoint;
  CPoint xMaxPoint;
  CPoint yMinPoint;
  CPoint yMaxPoint;

  float curveXmin, curveXmax;
  float curveYmin, curveYmax;

  itCurrentCurve = m_LevelCurves.begin();
  itLastCurve = m_LevelCurves.end();

  curveXmin = curveXmax = (*itCurrentCurve)->getControlPoints()[0]->X();
  curveYmin = curveYmax = (*itCurrentCurve)->getControlPoints()[0]->Z();


  while (itCurrentCurve != itLastCurve)
  {
    itCurrentPoint = (*itCurrentCurve)->getControlPoints().begin();
    itLastPoint = (*itCurrentCurve)->getControlPoints().end();

    while (itCurrentPoint != itLastPoint)
    {
      xtmp = (*itCurrentPoint)->X();
      ytmp = (*itCurrentPoint)->Z();

      // Find x-most, x-less, y-most, y-less
      if (xtmp > curveXmax)
      {
        curveXmax = xtmp;
        xMaxPoint = *(*itCurrentPoint);
      }

      if (xtmp < curveXmin)
      {
        curveXmin = xtmp;
        xMinPoint = *(*itCurrentPoint);
      }

      if (ytmp > curveYmax)
      {
        curveYmax = ytmp;
        yMaxPoint = *(*itCurrentPoint);
      }

      if (ytmp < curveYmin)
      {
        curveYmin = ytmp;
        yMinPoint = *(*itCurrentPoint);
      }

      itCurrentPoint++;
    }
    itCurrentCurve++;
  }
  /**/


  //*******************************************************************
  // Create splines from extremums
  CLevelCurve * tmpCurve;
  deque<CPoint *>::iterator itExtremum = m_Extremums.begin();
  deque<CPoint *>::iterator itLastExtremum = m_Extremums.end();
  while (itExtremum != itLastExtremum)
  {
    for (float angle = 0; angle <= PI; angle += PI/16.0)
    {
      float x = cos(angle);
      float y = sin(angle);
      tmpCurve = castNewLevelCurve (CPoint((*itExtremum)->X()+x, 0.0 , (*itExtremum)->Z()+y), CPoint((*itExtremum)->X()-x, 0.0 , (*itExtremum)->Z()-y), (*itExtremum));
      if (tmpCurve)
      {
        m_Couples.push_back (tmpCurve);
      }
    }

    itExtremum++;
  }


  // Second pass : recast added level curves using all level curves
  deque<CLevelCurve *>::iterator itCurrentCurve = m_Couples.begin();
  deque<CLevelCurve *>::iterator itLastCurve = m_Couples.end();
  while (itCurrentCurve!= itLastCurve)
  {
    recastCouple (*itCurrentCurve);
    itCurrentCurve++;
  }


  //*******************************************************************
  // Calculate height of each heigthmap componant
  /**/
  float xOffset = m_HeightMap->getXOffset ();
  float yOffset = m_HeightMap->getYOffset ();

  m_BuildingSplines.clear ();
  m_ContactPoints.clear ();

  CSpline * tmpSpline;
  deque<CPoint * > pointList;
  CPoint hmPoint;
  CPoint hmPointVertical;
  CPoint tmpPoint1;
  CPoint tmpPoint2;

  for (int xIndex = 0; xIndex < m_HeightMap->getXSize(); xIndex++)
  {
    //Build spline
    pointList.clear();

    // get intersection points with level curves
    itCurrentCurve = m_LevelCurves.begin();
    itLastCurve = m_LevelCurves.end();
    while (itCurrentCurve != itLastCurve)
    {
      tmpPoint1 = CPoint(m_HeightMap->getXMin() + (float)xIndex * xOffset + xOffset/2.0, 0.0, m_HeightMap->getYMin());
      tmpPoint2 = CPoint(m_HeightMap->getXMin() + (float)xIndex * xOffset + xOffset/2.0, 0.0, m_HeightMap->getYMax());
      (*itCurrentCurve)->get2DIntersections (tmpPoint1, tmpPoint2, pointList, 0, 2, 1);
      itCurrentCurve++;
    }

    // get intersection points with couples
    itCurrentCurve = m_Couples.begin();
    itLastCurve = m_Couples.end();
    while (itCurrentCurve != itLastCurve)
    {
      tmpPoint1 = CPoint(m_HeightMap->getXMin() + (float)xIndex * xOffset + xOffset/2.0, 0.0, m_HeightMap->getYMin());
      tmpPoint2 = CPoint(m_HeightMap->getXMin() + (float)xIndex * xOffset + xOffset/2.0, 0.0, m_HeightMap->getYMax());
      (*itCurrentCurve)->get2DIntersections (tmpPoint1, tmpPoint2, pointList, 0, 2, 1);
      itCurrentCurve++;
    }

    // Create corresponding spline.
    if (pointList.size()>=4)
    {
      tmpSpline = new CSpline (m_HeightMap->getXSize()+10);
      //tmpSpline->adaptativeTension(true);
      tmpSpline->addControlPoints (pointList);
      tmpSpline->reachAllPoints(true);
      tmpSpline->Build();
      m_BuildingSplines.push_back(tmpSpline);

      m_ContactPoints.insert(m_ContactPoints.end(), pointList.begin(), pointList.end());

      for (int yIndex = 0; yIndex < m_HeightMap->getYSize(); yIndex++)
      {
        pointList.clear ();
        hmPoint = CPoint(m_HeightMap->getXMin() + xIndex * xOffset + xOffset/2.0, 0.0, m_HeightMap->getYMin() + yIndex * yOffset + yOffset/2.0);
        hmPointVertical = hmPoint + CPoint (0.0, 1.0, 0.0);
        tmpSpline->get2DIntersections (hmPoint, hmPointVertical, pointList, 2, 1, 0);
        (*m_HeightMap)[xIndex][yIndex] = pointList[0]->Y();
      }


    }
  }
  /**/
}

/****************************************************************************/
CSpline * CLevelTerrain::castSpline (CPoint Point1, CPoint Point2)
{
  CSpline * tmpSpline = NULL;

  deque<CPoint *> pointList;

  // Get intersection with provided level curves (planar curves)
  deque<CLevelCurve *>::iterator itCurrentCurve = m_LevelCurves.begin();
  deque<CLevelCurve *>::iterator itLastCurve = m_LevelCurves.end();
  while (itCurrentCurve != itLastCurve)
  {
    (*itCurrentCurve)->get2DIntersections (Point1, Point2, pointList, 0, 2, 1);
    itCurrentCurve++;
  }

  // Get intersections with added level curves (vertical curves)
  itCurrentCurve = m_Couples.begin();
  itLastCurve = m_Couples.end();
  while (itCurrentCurve != itLastCurve)
  {
    (*itCurrentCurve)->get2DIntersections (Point1, Point2, pointList, 0, 2, 1);
    itCurrentCurve++;
  }

  if (pointList.size()>=4)
  {
    tmpSpline = new CSpline (m_HeightMap->getXSize()+10, CColor (0.6, 0.0, 0.0, 1.0));
    tmpSpline->addControlPoints (pointList);
    tmpSpline->reachAllPoints(true);
    tmpSpline->Build();
  }

  return tmpSpline;
}

/****************************************************************************/
CLevelCurve * CLevelTerrain::castNewLevelCurve (CPoint Point1, CPoint Point2, CPoint * emittingExtremum)
{
  CLevelCurve * tmpCurve = NULL;

  deque<CPoint *> pointList;

  deque<CLevelCurve *>::iterator itCurrentCurve = m_LevelCurves.begin();
  deque<CLevelCurve *>::iterator itLastCurve = m_LevelCurves.end();

  while (itCurrentCurve != itLastCurve)
  {
    (*itCurrentCurve)->get2DIntersections (Point1, Point2, pointList, 0, 2, 1);
    itCurrentCurve++;
  }

  if (pointList.size()>=4)
  {
    tmpCurve = new CLevelCurve (m_HeightMap->getYSize()+10, 0.0, false, emittingExtremum);
    tmpCurve->addControlPoints (pointList);
    tmpCurve->reachAllPoints(true);
    tmpCurve->Build();
  }

  return tmpCurve;
}

/****************************************************************************/
void CLevelTerrain::recastCouple (CLevelCurve * couple)
{
  deque<CPoint *> pointList;

  CPoint Point1;
  CPoint Point2;

  Point1 = couple->getSplinePoint (0.0);
  Point2 = couple->getSplinePoint (1.0);

  // Get intersection points with level curves
  deque<CLevelCurve *>::iterator itCurve = m_LevelCurves.begin();;
  deque<CLevelCurve *>::iterator itLastCurve = m_LevelCurves.end();
  while (itCurve != itLastCurve)
  {
    (*itCurve)->get2DIntersections (Point1, Point2, pointList, 0, 2, 1);
    itCurve++;
  }

  // Get intersection points with couples related to nearest top
  itCurve = m_Couples.begin();
  itLastCurve = m_Couples.end();
  while (itCurve != itLastCurve)
  {
    if ((*itCurve) != couple)
    {
      (*itCurve)->getFiltered2DIntersections (Point1, Point2, pointList, this, 0, 2, 1);
    }
    itCurve++;
  }

  couple->reachAllPoints(false);
  couple->zeroControlPoints();
  couple->addControlPoints(pointList);
  couple->reachAllPoints(true);
}

/****************************************************************************/
void CLevelTerrain::CalculateHeightMapBounds ()
{
  float xMin, xMax;
  float yMin, yMax;
  float xtmp, ytmp;

  deque<CPoint *>::iterator itCurrentPoint;
  deque<CPoint *>::iterator itLastPoint;

  deque<CLevelCurve*>::iterator itCurrentCurve = m_LevelCurves.begin();
  deque<CLevelCurve*>::iterator itLastCurve = m_LevelCurves.end();

  xMin = xMax = (*itCurrentCurve)->getControlPoints()[0]->X();
  yMin = yMax = (*itCurrentCurve)->getControlPoints()[0]->Z();

  while (itCurrentCurve != itLastCurve)
  {
    itCurrentPoint = (*itCurrentCurve)->getControlPoints().begin();
    itLastPoint = (*itCurrentCurve)->getControlPoints().end();

    while (itCurrentPoint != itLastPoint)
    {
      xtmp = (*itCurrentPoint)->X();
      ytmp = (*itCurrentPoint)->Z();

      if (xtmp > xMax)
        xMax = xtmp;

      if (xtmp < xMin)
        xMin = xtmp;

      if (ytmp > yMax)
        yMax = ytmp;

      if (ytmp < yMin)
        yMin = ytmp;


      itCurrentPoint++;
    }

    m_HeightMap->setBounds(xMin, xMax, yMin, yMax);
    itCurrentCurve++;
  }
}

/****************************************************************************/
void CLevelTerrain::CalculateExtremums ()
{
  deque<CLevelCurve *>::iterator itCurrentCurve = m_LevelCurves.begin();
  deque<CLevelCurve *>::iterator itLastCurve = m_LevelCurves.end();

  while (itCurrentCurve != itLastCurve)
  {
    if ((*itCurrentCurve)->IsExtremum())
    {
      (*itCurrentCurve)->Build();
      m_Extremums.push_back((*itCurrentCurve)->getCenter());
    }

    itCurrentCurve++;
  }
}
