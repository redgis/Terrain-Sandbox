/*
 * terracurv.h
 *
 *  Created on: May 26, 2010
 *      Author: Régis Martinez - regis.martinez3@free.fr
 */

#ifndef TERRACURV_H_
#define TERRACURV_H_

#include "../common/spline.hpp"
#include "../common/camera.hpp"
#include "levelterrain.hpp"
#include "levelcurve.hpp"

#define GUI_FULLSCREEN 0
#define GUI_WIREFRAME 1
#define GUI_TERRAIN_SIZE 2
#define GUI_REDBUILD_TERRAIN 3
#define GUI_SHOW_NORMALS 4
#define GUI_SHOW_LEVEL_CURVES 5
#define GUI_SHOW_RADIAL_SPLINES 6
#define GUI_SMOOTH_OR_FLAT 7
#define GUI_USE_TRIANGLES 8
#define GUI_SHOW_HEIGHTMAP 9
#define GUI_SHOW_TERRAIN 10

// Window IDs
int terraMainWindow;

// Cameras
CCamera * myCamera;

// GUI
GLUI * myGui;

//GUI options
int doWireFrame = 0;
int oldWidth, oldHeight;
int myTerrainSize = 50;
int showNormals = 0;
int showLevelCurves = 0;
int showRadialSplines = 0;
int doSmooth = 1;
int useTriangles = 1;
int showHeightMap = 1;
int showTerrain = 1;

// Keaybord and mouse interactions
float MoveStep;
int Xold, Yold;
char Presse;

// Splines
CLevelCurve * myLevelCurve;
float CatmullRomS;

// Lights & Material
GLfloat light0_ambient[] =  {0.1f, 0.1f, 0.1f, 1.0f};
GLfloat light0_diffuse[] =  {.6f, 0.6f, 0.6f, 1.0f};
GLfloat light0_position[] = {5.0f, 5.0f, 10.0f, 0.0f};
GLfloat lights_rotation[16] = {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1 };

GLfloat material0_ambient[] = { 1.0, 1.0, 1.0, 1.0 };
GLfloat material0_specular[] = { 1.0, 1.0, 1.0, 1.0 };

// Heightmap
CLevelTerrain * myLevelTerrain;


#endif /* TERRACURV_H_ */
