/*
 * levelcurve.h
 *
 *  Created on: May 27, 2010
 *      Author: Regis Martinez - regis.martinez3@free.fr
 */

#ifndef LEVELCURVE_H_
#define LEVELCURVE_H_

#include "../common/spline.hpp"

class CLevelTerrain;

class CLevelCurve : public CSpline
{
  private:
    float m_Height;
    bool m_IsExtremum;
    CPoint * m_EmittingExtremum;

  public:
    CLevelCurve(int Discretization = 10, float Height = 0.0, bool Extremum = false, CPoint * emitExtremum = NULL);
    virtual  ~CLevelCurve();

    virtual void addPoint (CPoint * newPoint);
    virtual void addPoint (CPoint & newPoint);
    virtual void addPoint (float x, float z);

    float getHeight ();
    CPoint * getCenter ();
    CPoint * getEmittingExtremum();
    bool IsExtremum ();

    int getFiltered2DIntersections (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, CLevelTerrain * levelTerrain, int Axis0 = 0, int Axis1 = 1, int Axis2 = 2, bool lineIntersection = true);

    virtual void Build ();
    virtual void Draw ();
};

#endif /* LEVELCURVE_H_ */
