/*
 * heightmesh.h
 *
 *  Created on: May 28, 2010
 *      Author: regis
 */

#ifndef TERRAINMESH_H_
#define TERRAINMESH_H_

#include "../common/drawable.hpp"
#include "heightmap.hpp"

class CTerrainMesh : public CDrawable
{

   protected:
      CHeightMap * m_HeightMap;
      bool m_drawNormals;
      bool m_useTriangles;
      bool m_drawSea;

   public:
      CTerrainMesh(CHeightMap * hm = 0);
      virtual ~CTerrainMesh();

      void drawNormals(bool);
      bool drawNormals();

      void useTriangles(bool);
      bool useTriangles();

      void drawSea(bool newVal);
      bool drawSea();

      void setHeightMap(CHeightMap * hm);
      CHeightMap * getHeightMap();

      void Build();
      void Draw();

   private:
      void setWaterColor(float depth, float deepest);
};

#endif /* TERRAINMESH_H_ */
