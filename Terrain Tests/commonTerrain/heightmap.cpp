/*
 * heightmap.cpp
 *
 *  Created on: May 27, 2010
 *      Author: regis
 */

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <GL/glut.h>
#include "heightmap.hpp"
#include "../common/point.hpp"
#include "../common/tools3d.hpp"

using namespace std;

/****************************************************************************/
CHeightMap::CHeightMap (int newXSize, int newYSize, int xMin, int xMax, int yMin, int yMax)
{
  m_drawGrid = false;
  m_inHud = true;

  m_xMin = xMin;
  m_xMax = xMax;
  m_yMin = yMin;
  m_yMax = yMax;

  m_xSize = newXSize;
  m_ySize = newYSize;

  m_xOffset = (m_xMax - m_xMin) / (float) m_xSize;
  m_yOffset = (m_yMax - m_yMin) / (float) m_ySize;

  
  m_HeightMap = new float[m_xSize*m_ySize];

  //srand (time(NULL));

  for (int Index = 0; Index < m_xSize*m_ySize; Index++)
  {
    //m_HeightMap[Index] = ((float) rand ()/(float)RAND_MAX) * 100.0;
    //m_HeightMap[Index] = 0.4 * (cos ( (float) 10.0 * (float) (Index % m_xSize) / (float)m_xSize) * sin ( (float) 20.0*((float)Index/(float)m_ySize) / (float) m_ySize));
    m_HeightMap[Index] = -0.1f;
  }

}

/****************************************************************************/
CHeightMap::~CHeightMap ()
{
  delete [] m_HeightMap;
}

/****************************************************************************/
float * CHeightMap::HeightMap ()
{
  return m_HeightMap;
}

/****************************************************************************/
void CHeightMap::setSize (int newXVal, int newYVal)
{
  m_xSize = newXVal;
  m_ySize = newYVal;

  delete m_HeightMap;

  m_HeightMap = new float[m_xSize*m_ySize];

  for (int Index = 0; Index < m_xSize*m_ySize; Index++)
  {
    m_HeightMap[Index] = 0;
  }

  m_xOffset = (m_xMax - m_xMin) / (float) m_xSize;
  m_yOffset = (m_yMax - m_yMin) / (float) m_ySize;
}

/****************************************************************************/
int CHeightMap::getXSize ()
{
  return m_xSize;
}

/****************************************************************************/
int CHeightMap::getYSize ()
{
  return m_ySize;
}

/****************************************************************************/
void CHeightMap::setBounds (int xMin, int xMax, int yMin, int yMax)
{
  m_xMin = xMin;
  m_xMax = xMax;
  m_yMin = yMin;
  m_yMax = yMax;

  m_xOffset = (m_xMax - m_xMin) / (float) m_xSize;
  m_yOffset = (m_yMax - m_yMin) / (float) m_ySize;
}

/****************************************************************************/
float CHeightMap::getXMin ()
{
  return m_xMin;
}

/****************************************************************************/
float CHeightMap::getXMax ()
{
  return m_xMax;
}

/****************************************************************************/
float CHeightMap::getYMin ()
{
  return m_yMin;
}

/****************************************************************************/
float CHeightMap::getYMax ()
{
  return m_yMax;
}

/****************************************************************************/
void CHeightMap::Build ()
{
  if (glIsList (m_idDrawList))
    glDeleteLists (m_idDrawList, 1);

  glNewList(m_idDrawList, GL_COMPILE);
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glPushMatrix();
  glDisable (GL_LIGHTING);

  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  if(m_inHud)
  {
    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();

    glRotatef (90.0, 1.0, 0.0, 0.0);
    glTranslatef (0.5, -1.0, 0.78);
    glScalef (0.02, 0.02, 0.02);
  }

    // get highest and smallest value.
    float maxHeight = m_HeightMap[0];
    float minHeight = m_HeightMap[0];
    for (int Index = 1; Index < m_xSize*m_ySize; Index++)
    {
      if (m_HeightMap[Index] > maxHeight)
        maxHeight = m_HeightMap[Index];

      if (m_HeightMap[Index] < minHeight)
         minHeight = m_HeightMap[Index];
    }


    // graw lines around heighmap
    glBegin (GL_LINE_STRIP);
    glColor3f(1.0, 1.0, 1.0);
    glVertex3f(m_xMin, 0.0, m_yMax);
    glVertex3f(m_xMax, 0.0, m_yMax);
    glVertex3f(m_xMax, 0.0, m_yMin);
    glVertex3f(m_xMin, 0.0, m_yMin);
    glVertex3f(m_xMin, 0.0, m_yMax);
    glEnd();

    CPoint Vertices[14];

    // Display Height map ground color
    for (int xIndex = 0; xIndex <= m_xSize-1; xIndex++)
    {
      glBegin(GL_QUADS);
      for (int yIndex = 0; yIndex <= m_ySize-1; yIndex++)
      {
        //glColor4f (((*this)[xIndex][yIndex] - minHeight)/maxHeight, ((*this)[xIndex][yIndex] - minHeight)/maxHeight, ((*this)[xIndex][yIndex] - minHeight)/maxHeight, 0.7f);
        //glColor4f (((*this)[xIndex][yIndex] - minHeight)/maxHeight, 0.0, 0.0, 0.5f);
        glColor4f (0.0, ((*this)[xIndex][yIndex] - minHeight)/maxHeight, 0.0, 0.8f);

        Vertices[0] = CPoint (m_xMin + xIndex * m_xOffset, 0.0, m_yMin + yIndex * m_yOffset);
        Vertices[1] = CPoint (m_xMin + (xIndex + 1) * m_xOffset, 0.0, m_yMin + yIndex * m_yOffset);
        Vertices[2] = CPoint (m_xMin + (xIndex + 1) * m_xOffset, 0.0, m_yMin + (yIndex + 1) * m_yOffset);
        Vertices[3] = CPoint (m_xMin + xIndex * m_xOffset, 0.0, m_yMin + (yIndex + 1) * m_yOffset);


        glNormal3fv (CPoint (CalculateNormal(Vertices[0], Vertices[1], Vertices[3])).xyzwArray());
        glVertex3fv (Vertices[0].xyzwArray());
        glVertex3fv (Vertices[1].xyzwArray());
        glVertex3fv (Vertices[2].xyzwArray());
        glVertex3fv (Vertices[3].xyzwArray());
      }
      glEnd();
    }



    // Display grid
    if (m_drawGrid)
    {
      glColor4f (0.4, 0.4, 0.4, 0.7);

      glBegin(GL_LINES);
      for (float x = m_xMin; x <= m_xMax; x += m_xOffset)
      {
        glVertex3f (x, 0.0, m_yMin);
        glVertex3f (x, 0.0, m_yMax);
      }

      for (float y = m_yMin; y <= m_yMax; y += m_yOffset)
      {
        glVertex3f (m_xMin, 0.0, y);
        glVertex3f (m_xMax, 0.0, y);
      }
      glEnd();
    }

  glEnable (GL_LIGHTING);
  glPopMatrix();
  glPopAttrib();
  glEndList();
}

/****************************************************************************/
void CHeightMap::Draw ()
{
  glDisable (GL_DEPTH_TEST);
  glDepthMask (GL_FALSE);
  glEnable(GL_BLEND) ;
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  CDrawable::Draw ();

  glDisable(GL_BLEND);
  glDepthMask (GL_TRUE);
  glEnable(GL_DEPTH_TEST);
}

/****************************************************************************/
float CHeightMap::getXOffset ()
{
  return m_xOffset;
}

/****************************************************************************/
float CHeightMap::getYOffset ()
{
  return m_yOffset;
}

/****************************************************************************/
bool CHeightMap::gridIsDrawable ()
{
  return m_drawGrid;
}

/****************************************************************************/
void CHeightMap::drawGrid (bool newVal)
{
  m_drawGrid = newVal;
}

/****************************************************************************/
float * CHeightMap::operator[] (size_t pos)
{
  return m_HeightMap+(m_ySize*pos);
}

