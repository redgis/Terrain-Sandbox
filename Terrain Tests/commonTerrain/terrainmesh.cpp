/*
 * heightmesh.cpp
 *
 *  Created on: May 28, 2010
 *      Author: regis
 */

#include <GL/glut.h>
#include "terrainmesh.hpp"
#include "../common/tools3d.hpp"
#include "../common/point.hpp"
#include <algorithm>

 /*****************************************************************************/
CTerrainMesh::CTerrainMesh(CHeightMap * hm) : m_HeightMap(hm)
{
   m_drawNormals = false;
   m_useTriangles = true;
   m_drawSea = true;
}

/*****************************************************************************/
CTerrainMesh::~CTerrainMesh()
{
   //nothing to clean
   //TODO : terrain mesh does not own the heightmap. Should remove the heightmap from members to avoid destruction. Only give heightmap pointer as a parmater to build method for example.
}

/*****************************************************************************/
void CTerrainMesh::drawNormals(bool newVal)
{
   m_drawNormals = newVal;
}

/*****************************************************************************/
bool CTerrainMesh::drawNormals()
{
   return m_drawNormals;
}

/*****************************************************************************/
bool CTerrainMesh::useTriangles()
{
   return m_useTriangles;
}

/*****************************************************************************/
void CTerrainMesh::useTriangles(bool val)
{
   m_useTriangles = val;
}

/*****************************************************************************/
bool CTerrainMesh::drawSea()
{
   return m_drawSea;
}

/*****************************************************************************/
void CTerrainMesh::drawSea(bool newVal)
{
   m_drawSea = newVal;
}


/*****************************************************************************/
void CTerrainMesh::setHeightMap(CHeightMap * hm)
{
   m_HeightMap = hm;
}

/*****************************************************************************/
CHeightMap * CTerrainMesh::getHeightMap()
{
   return m_HeightMap;
}

/*****************************************************************************/
void CTerrainMesh::Build()
{
   if (glIsList(m_idDrawList))
      glDeleteLists(m_idDrawList, 1);

   glNewList(m_idDrawList, GL_COMPILE);
   glPushMatrix();
   glPushAttrib(GL_ALL_ATTRIB_BITS);

   int xSize = m_HeightMap->m_xSize;
   int ySize = m_HeightMap->m_ySize;
   float xOffset = m_HeightMap->m_xOffset;
   float yOffset = m_HeightMap->m_yOffset;
   float xMin = m_HeightMap->m_xMin + xOffset / 2.0;
   //float xMax = m_HeightMap->m_xMax + xOffset/2.0;
   float yMin = m_HeightMap->m_yMin + yOffset / 2.0;
   //float yMax = m_HeightMap->m_yMax + yOffset/2.0;

   //float * hm = m_HeightMap->m_HeightMap;


   CPoint Vertices[16];
   CPoint Normals[16];
   CPoint tmpPoint;

   glEnable(GL_COLOR_MATERIAL);
   
   float highest = 0.0f;
   float lowest = 0.0f;

   // Find Min and Max
   for (int xIndex = 0; xIndex < xSize - 2; xIndex++) {
      for (int yIndex = 0; yIndex < ySize - 2; yIndex++) {
         highest = (*m_HeightMap)[xIndex][yIndex] > highest ?
            (*m_HeightMap)[xIndex][yIndex] : highest;

         lowest = (*m_HeightMap)[xIndex][yIndex] < lowest ?
            (*m_HeightMap)[xIndex][yIndex] : lowest;
      }
   }

   // Display Height map ground color
   for (int xIndex = 0; xIndex < xSize - 2; xIndex++) {
      for (int yIndex = 0; yIndex < ySize - 2; yIndex++) {
         glColor4f(1.0f, 1.0f, 1.0f, 1.0);

         Vertices[0] = CPoint(xMin + xIndex * xOffset, (*m_HeightMap)[xIndex][yIndex], yMin + yIndex * yOffset);
         Vertices[1] = CPoint(xMin + (xIndex + 1) * xOffset, (*m_HeightMap)[xIndex + 1][yIndex], yMin + yIndex * yOffset);
         Vertices[2] = CPoint(xMin + (xIndex + 1) * xOffset, (*m_HeightMap)[xIndex + 1][yIndex + 1], yMin + (yIndex + 1) * yOffset);
         Vertices[3] = CPoint(xMin + xIndex * xOffset, (*m_HeightMap)[xIndex][yIndex + 1], yMin + (yIndex + 1) * yOffset);

         Normals[4] = CPoint(CalculateNormal(Vertices[0], Vertices[3], Vertices[1]));
         Normals[4].Normalize();
         tmpPoint = CPoint(CalculateNormal(Vertices[1], Vertices[3], Vertices[2]));
         tmpPoint.Normalize();
         Normals[4] += tmpPoint;
         Normals[4].Normalize();
         Normals[0] = Normals[4];
         Normals[1] = Normals[4];
         Normals[2] = Normals[4];
         Normals[3] = Normals[4];

         if (xIndex >= 1) {
            Vertices[4] = CPoint(xMin + (xIndex - 1) * xOffset, (*m_HeightMap)[xIndex - 1][yIndex + 1], yMin + (yIndex + 1) * yOffset);
            Vertices[5] = CPoint(xMin + (xIndex - 1) * xOffset, (*m_HeightMap)[xIndex - 1][yIndex], yMin + yIndex * yOffset);
            Normals[5] = CPoint(CalculateNormal(Vertices[5], Vertices[4], Vertices[0]));
            Normals[5].Normalize();
            tmpPoint = CPoint(CalculateNormal(Vertices[0], Vertices[4], Vertices[3]));
            tmpPoint.Normalize();
            Normals[5] += tmpPoint;
            Normals[5].Normalize();
            Normals[0] += Normals[5];
            Normals[3] += Normals[5];
         }

         if (yIndex >= 1) {
            Vertices[6] = CPoint(xMin + xIndex * xOffset, (*m_HeightMap)[xIndex][yIndex - 1], yMin + (yIndex - 1) * yOffset);
            Vertices[7] = CPoint(xMin + (xIndex + 1) * xOffset, (*m_HeightMap)[xIndex + 1][yIndex - 1], yMin + (yIndex - 1) * yOffset);
            Normals[6] = CPoint(CalculateNormal(Vertices[6], Vertices[0], Vertices[7]));
            Normals[6].Normalize();
            tmpPoint = CPoint(CalculateNormal(Vertices[7], Vertices[0], Vertices[1]));
            tmpPoint.Normalize();
            Normals[6] += tmpPoint;
            Normals[6].Normalize();
            Normals[0] += Normals[6];
            Normals[1] += Normals[6];
         }

         if (xIndex <= xSize - 2) {
            Vertices[8] = CPoint(xMin + (xIndex + 2) * xOffset, (*m_HeightMap)[xIndex + 2][yIndex], yMin + yIndex * yOffset);
            Vertices[13] = CPoint(xMin + (xIndex + 2) * xOffset, (*m_HeightMap)[xIndex + 2][yIndex + 1], yMin + (yIndex + 1) * yOffset);
            Normals[7] = CPoint(CalculateNormal(Vertices[1], Vertices[2], Vertices[8]));
            Normals[7].Normalize();
            tmpPoint = CPoint(CalculateNormal(Vertices[8], Vertices[2], Vertices[13]));
            tmpPoint.Normalize();
            Normals[7] += tmpPoint;
            Normals[7].Normalize();
            Normals[1] += Normals[7];
            Normals[2] += Normals[7];
         }

         if (yIndex <= ySize - 2) {
            Vertices[9] = CPoint(xMin + xIndex * xOffset, (*m_HeightMap)[xIndex][yIndex + 2], yMin + (yIndex + 2) * yOffset);
            Vertices[14] = CPoint(xMin + (xIndex + 1) * xOffset, (*m_HeightMap)[xIndex + 1][yIndex + 2], yMin + (yIndex + 2) * yOffset);
            Normals[8] = CPoint(CalculateNormal(Vertices[3], Vertices[9], Vertices[2]));
            Normals[8].Normalize();
            tmpPoint = CPoint(CalculateNormal(Vertices[2], Vertices[9], Vertices[14]));
            tmpPoint.Normalize();
            Normals[8] += tmpPoint;
            Normals[8].Normalize();
            Normals[3] += Normals[8];
            Normals[2] += Normals[8];
         }

         if ((xIndex >= 1) && (yIndex >= 1)) {
            Vertices[11] = CPoint(xMin + (xIndex - 1) * xOffset, (*m_HeightMap)[xIndex - 1][yIndex - 1], yMin + (yIndex - 1) * yOffset);
            Normals[10] = CPoint(CalculateNormal(Vertices[11], Vertices[5], Vertices[6]));
            Normals[10].Normalize();
            tmpPoint = CPoint(CalculateNormal(Vertices[6], Vertices[5], Vertices[0]));
            tmpPoint.Normalize();
            Normals[10] += tmpPoint;
            Normals[10].Normalize();
            Normals[0] += Normals[10];
         }

         if ((xIndex >= 1) && (yIndex <= ySize - 2)) {
            Vertices[10] = CPoint(xMin + (xIndex - 1) * xOffset, (*m_HeightMap)[xIndex - 1][yIndex + 2], yMin + (yIndex + 2) * yOffset);
            Normals[9] = CPoint(CalculateNormal(Vertices[4], Vertices[10], Vertices[3]));
            Normals[9].Normalize();
            tmpPoint = CPoint(CalculateNormal(Vertices[3], Vertices[10], Vertices[9]));
            tmpPoint.Normalize();
            Normals[9] += tmpPoint;
            Normals[9].Normalize();
            Normals[3] += Normals[9];
         }

         if ((xIndex <= xSize - 2) && (yIndex >= 1)) {
            Vertices[12] = CPoint(xMin + (xIndex + 2) * xOffset, (*m_HeightMap)[xIndex + 2][yIndex - 1], yMin + (yIndex - 1) * yOffset);
            Normals[11] = CPoint(CalculateNormal(Vertices[7], Vertices[1], Vertices[12]));
            Normals[11].Normalize();
            tmpPoint = CPoint(CalculateNormal(Vertices[12], Vertices[1], Vertices[8]));
            tmpPoint.Normalize();
            Normals[11] += tmpPoint;
            Normals[11].Normalize();
            Normals[1] += Normals[11];
         }

         if ((xIndex <= xSize - 2) && (yIndex <= ySize - 2)) {
            Vertices[13] = CPoint(xMin + (xIndex + 2) * xOffset, (*m_HeightMap)[xIndex + 2][yIndex + 1], yMin + (yIndex + 1) * yOffset);
            Vertices[14] = CPoint(xMin + (xIndex + 1) * xOffset, (*m_HeightMap)[xIndex + 1][yIndex + 2], yMin + (yIndex + 2) * yOffset);
            Vertices[15] = CPoint(xMin + (xIndex + 2) * xOffset, (*m_HeightMap)[xIndex + 2][yIndex + 2], yMin + (yIndex + 2) * yOffset);
            Normals[12] = CPoint(CalculateNormal(Vertices[2], Vertices[14], Vertices[13]));
            Normals[12].Normalize();
            tmpPoint = CPoint(CalculateNormal(Vertices[13], Vertices[14], Vertices[15]));
            tmpPoint.Normalize();
            Normals[12] += tmpPoint;
            Normals[12].Normalize();
            Normals[2] += Normals[12];
         }

         Normals[0].Normalize();
         Normals[1].Normalize();
         Normals[2].Normalize();
         Normals[3].Normalize();

         if (m_drawNormals) {
            glDisable(GL_LIGHTING);
            glBegin(GL_LINES);
            glColor3f(1.0, 0.0, 0.0);
            glVertex3fv(Vertices[0].xyzwArray());
            glVertex3fv((Vertices[0] + Normals[0] * 0.2).xyzwArray());

            glColor3f(0.0, 1.0, 0.0);
            glVertex3fv(Vertices[1].xyzwArray());
            glVertex3fv((Vertices[1] + Normals[1] * 0.2).xyzwArray());

            glColor3f(0.0, 0.0, 1.0);
            glVertex3fv(Vertices[2].xyzwArray());
            glVertex3fv((Vertices[2] + Normals[2] * 0.2).xyzwArray());

            glColor3f(1.0, 0.0, 1.0);
            glVertex3fv(Vertices[3].xyzwArray());
            glVertex3fv((Vertices[3] + Normals[3] * 0.2).xyzwArray());
            glEnd();
            glEnable(GL_LIGHTING);
         }

         if (m_useTriangles)
            glBegin(GL_TRIANGLE_STRIP);
         else
            glBegin(GL_QUADS);

         glNormal3fv(Normals[0].xyzwArray());
         glVertex3fv(Vertices[0].xyzwArray());

         glNormal3fv(Normals[1].xyzwArray());
         glVertex3fv(Vertices[1].xyzwArray());

         if (m_useTriangles) {
            glNormal3fv(Normals[3].xyzwArray());
            glVertex3fv(Vertices[3].xyzwArray());

            glNormal3fv(Normals[2].xyzwArray());
            glVertex3fv(Vertices[2].xyzwArray());
         } else {
            glNormal3fv(Normals[2].xyzwArray());
            glVertex3fv(Vertices[2].xyzwArray());

            glNormal3fv(Normals[3].xyzwArray());
            glVertex3fv(Vertices[3].xyzwArray());
         }

         glEnd();
      }
   }


   if (m_drawSea) {

      glEnable(GL_BLEND);
      glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

      glBegin(GL_QUADS);

      float depthRatio = 0.0f;
      float waterColorGradient = 0.0f;

      for (int xIndex = 0; xIndex <= xSize - 2; xIndex++) {
         for (int yIndex = 0; yIndex <= ySize - 2; yIndex++) {

            setWaterColor((*m_HeightMap)[xIndex][yIndex], lowest);
            glVertex3f(xMin + xIndex * xOffset, -0.0f, yMin + yIndex * yOffset);
            glNormal3f(0.0f, 1.0f, 0.0f);

            setWaterColor((*m_HeightMap)[xIndex][yIndex + 1], lowest);
            glVertex3f(xMin + xIndex * xOffset, -0.0f, yMin + (yIndex + 1) * yOffset);
            glNormal3f(0.0f, 1.0f, 0.0f);


            setWaterColor((*m_HeightMap)[xIndex + 1][yIndex + 1], lowest);
            glVertex3f(xMin + (xIndex + 1) * xOffset, -0.0f, yMin + (yIndex + 1) * yOffset);
            glNormal3f(0.0f, 1.0f, 0.0f);

            setWaterColor((*m_HeightMap)[xIndex + 1][yIndex], lowest);
            glVertex3f(xMin + (xIndex + 1) * xOffset, -0.0f, yMin + yIndex * yOffset);
            glNormal3f(0.0f, 1.0f, 0.0f);

         }
      }

      glEnd();

      glDisable(GL_BLEND);
   }

   glDisable(GL_COLOR_MATERIAL);

   
   glPopMatrix();
   glPopAttrib();
   glEndList();
}

// Get a color for sea, that depends on deepness : the deepest the darkest
void CTerrainMesh::setWaterColor(float depth, float deepest) {
   float depthRatio = depth <= 0.0f ?  // 0.0 : surface, 1.0 : bottom
      depth / deepest : 0.0f;
   float waterColorGradient = 0.01f * (1.0f - depthRatio);
   glColor4f(
      0.0f + waterColorGradient,
      0.1f + waterColorGradient,
      0.2f + waterColorGradient,
      1.1f * pow(depthRatio, 0.07f));
}

/*****************************************************************************/
void CTerrainMesh::Draw()
{
   CDrawable::Draw();
}
