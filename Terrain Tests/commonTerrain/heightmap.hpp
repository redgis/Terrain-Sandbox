/*
 * heightmap.h
 *
 *  Created on: May 27, 2010
 *      Author: regis
 */

#ifndef HEIGHTMAP_H_
#define HEIGHTMAP_H_

#include "../common/drawable.hpp"

class CTerrainMesh;

class CHeightMap : public CDrawable
{
  protected:

    float m_xMin;
    float m_xMax;
    float m_yMin;
    float m_yMax;

    int m_xSize;
    int m_ySize;

    float m_xOffset;
    float m_yOffset;

    float * m_HeightMap;

    bool m_drawGrid;
    bool m_inHud;

  public:
    CHeightMap(int newXVal = 100, int newYVal = 100, int xMin = -10, int xMax = 10, int yMin = -10, int yMax = 10);
    virtual ~CHeightMap();

    float * HeightMap ();

    void setSize (int, int);
    int getXSize ();
    int getYSize ();

    void setBounds (int xMin, int xMax, int yMin, int yMax);
    float getXMin ();
    float getXMax ();
    float getYMin ();
    float getYMax ();

    float getXOffset ();
    float getYOffset ();

    bool gridIsDrawable();
    void drawGrid (bool);

    void Build ();
    void Draw ();

    friend class CTerrainMesh;

    float * operator[] (size_t pos);
};

#endif /* HEIGHTMAP_H_ */
