/*
 * tools3d.h
 *
 *  Created on: May 28, 2010
 *      Author: regis
 */

#ifndef TOOLS3D_H_
#define TOOLS3D_H_

#ifndef PI
# define PI 3.141592
#endif


#include "point.hpp"

float * CalculateNormal (CPoint & Point1, CPoint & Point2, CPoint & Point3);

CPoint * Intersect2DSegments (CPoint & pt1, CPoint & pt2, CPoint & pt3, CPoint & pt4);
CPoint * Intersect3DSegments (CPoint & pt1, CPoint & pt2, CPoint & pt3, CPoint & pt4);

CPoint * Intersect2DLines (CPoint & pt1, CPoint & pt2, CPoint & pt3, CPoint & pt4);
CPoint * Intersect3DLines (CPoint & pt1, CPoint & pt2, CPoint & pt3, CPoint & pt4);

CPoint * Intersect2DLineToSegment (CPoint & pt1, CPoint & pt2, CPoint & pt3, CPoint & pt4);
CPoint * Intersect3DLineToSegment (CPoint & pt1, CPoint & pt2, CPoint & pt3, CPoint & pt4);

float VectorLength (CPoint & vect);
float DistancePointToPoint (CPoint & pt1, CPoint pt2);

float Noise2D(float x,float y);
#endif /* TOOLS3D_H_ */
