/***************************************************************************
 *            camera.h
 *
 *  Sat May 13 19:58:21 2006
 *  Copyright  2006  Regis Martinez
 *  regis.martinez@worldonline.fr
 ****************************************************************************/



#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <iostream>
using namespace std;

#include "point.hpp"

/****************************************************************************/
class CCamera
{
  private:
    CPoint m_Position;
    float m_Orientation;    // Angle aroung Y axis
    float m_Pitch;    // Pitch of the camera
    double m_FOV;      /* View angle (field of view) */
    double m_Aspect;        /* Aspect ratio of image (16/9ieme, 4/3 ...) */
    double m_Near;          /* Foreground plan */
    double m_Far;           /* Background plan */
    double m_Left;
    double m_Right;
    double m_Bottom;
    double m_Top;
    float m_RotationStep;  /* The rotation steps */
    bool m_Perspective;
    
    /* working variables */
    double m_RadOrientation;  /* FOV in radians */
    double m_RadPitch;  /* Pitch angle in radians */
    
  public:
    /* Constructors and destructor */
    CCamera ();
    CCamera (float X, float Y, float Z, float NewOrientation, float NewPitch);
    virtual ~CCamera ();
    
    /* Initialization */
    void InitCamera(double fovy, double aspect, double zNear,double zFar);
    
    /* Methodes de consultation */
    float X ();
    float Y ();
    float Z ();
    float GetOrientation ();
    float GetPitch ();
    double GetFOV ();
    double GetAspect ();
    double GetNear ();
    double GetFar ();
    double GetRight ();
    double GetLeft ();
    double GetBottom ();
    double GetTop ();
    float GetRotationStep ();
    bool GetPerspective ();
    
    /* Accessors methods */
    void SetXYZ (float NewX, float NewY, float NewZ);
    void SetX (float NewX);
    void SetY (float NewY);
    void SetZ (float NewZ);
    void SetOrientation (float NewOrientation);
    void SetPitch (float NewPitch);
    void SetFOV (double NewAV);
    void SetAspect (double NewAspect);
    void SetNear (double NewNear);
    void SetFar (double NewFar);
    void SetRight (double NewRight);
    void SetLeft (double NewLeft);
    void SetBottom (double NewBottom);
    void SetTop (double NewTop);
    void SetRotationStep (float NewPDR );
    void SetPerspective (bool val);
    
    /* Moving methods */
    void MoveRight (float MoveStep);
    void MoveLeft (float MoveStep);
    void MoveAhead (float MoveStep);
    void MoveBack (float MoveStep);
    void MoveUp(float MoveStep);
    void MoveDown (float MoveStep);
    void YawLeftRight (int Rotation);
    void Pitch (int Rotation);
    void ConvertDegRad ();
    void ApplyProjection ();
};

#endif /* _CAMERA_H_ */
