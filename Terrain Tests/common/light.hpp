/*
 * light.h
 *
 *  Created on: May 30, 2010
 *      Author: regis
 */

#ifndef LIGHT_H_
#define LIGHT_H_

#include "drawable.hpp"
#include "point.hpp"

class CLight : public CDrawable
{
  private:
    CPoint m_Position;

  public:
    CLight(float x = 0.0, float y = 5.0, float z = 0.0);
    CLight(CPoint & pt);
    virtual ~CLight();

};

#endif /* LIGHT_H_ */
