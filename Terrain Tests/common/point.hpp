#ifndef _POINT_H_
#define _POINT_H_

#include <iostream>
using namespace std;

//extern int count;


/****************************************************************************/
class CPoint
{
  private:
    /* Coordinates */
    union{
      struct{
        float x;
        float y;
        float z;
        float w;
      };
      float xyzw[4];
    };

  public:
    /* Constructor */
    CPoint (float NewX = 0.0, float NewY = 0.0, float NewZ = 0.0, float NewW = 1.0);
    CPoint (float * XYZW);
    CPoint (const CPoint &);
    CPoint (const CPoint *);
    virtual ~CPoint ();
    
    /* Modification methods */
    void SetXYZ (float NewX = 0.0, float NewY = 0.0, float NewZ = 0.0);
    void SetXYZW (float NewX = 0.0, float NewY = 0.0, float NewZ = 0.0, float NewW = 1.0);
    void SetX (float NewX);
    void SetY (float NewY);
    void SetZ (float NewZ);
    void SetW (float NewW);
    
    /* Accessors */
    float X ();
    float Y ();
    float Z ();
    float W ();
    float * xyzwArray ();


    void Normalize ();
    void gl (); // glVertex instruction

    friend CPoint operator+ (const CPoint &, const float &);
    friend CPoint operator- (const CPoint &, const float &);
    friend CPoint operator* (const CPoint &, const float &);
    friend CPoint operator/ (const CPoint &, const float &);

    friend CPoint operator+ (const CPoint &, const CPoint &);
    friend CPoint operator- (const CPoint &, const CPoint &);
    friend CPoint operator% (const CPoint &, const CPoint &);
    friend CPoint operator/ (const CPoint &, const CPoint &);
    friend CPoint operator^ (const CPoint &, const CPoint &); // vectorial product (get the normal to a plan defined by the two vectors
    friend float operator* (const CPoint &, const CPoint &); // scalar product (dot product)

    friend bool operator== (const CPoint &, const CPoint &);
    friend bool operator!= (const CPoint &, const CPoint &);

/*    Vector2D& Vector2D::operator+=(const Vector2D& right)
    {
        this->x += right.x;
        this->y += right.y;
        return *this;
    }
  */

    CPoint & operator+= (const float &);
    CPoint & operator-= (const float &);
    CPoint & operator*= (const float &);
    CPoint & operator/= (const float &);
    CPoint & operator= (const float &);

    CPoint & operator+= (const CPoint &);
    CPoint & operator-= (const CPoint &);
    CPoint & operator%= (const CPoint &);
    CPoint & operator/= (const CPoint &);
    CPoint & operator^= (const CPoint &);
    CPoint & operator= (const CPoint &);

    float & operator[] (size_t pos);

    friend ostream& operator << (ostream& out, const CPoint & pt);
};

#endif /* _POINT_H_ */
