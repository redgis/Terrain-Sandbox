/*
 * stltools.h
 *
 *  Created on: Jun 6, 2010
 *      Author: regis
 */

#ifndef STLTOOLS_H_
#define STLTOOLS_H_

#include <deque>
#include <iostream>
#include "point.hpp"

using namespace std;

/****************************************************************************/
template <typename CStuff>
void cleanStuffs (deque<CStuff *> & stuffs)
{
  typename deque<CStuff *>::iterator itStuff = stuffs.begin();
  typename deque<CStuff *>::iterator itLastStuff = stuffs.end();

  while (itStuff != itLastStuff)
  {
    if ((*itStuff) != NULL)
    {
      delete (*itStuff);
    }

    itStuff++;
  }

  stuffs.clear();
}


#endif /* STLTOOLS_H_ */
