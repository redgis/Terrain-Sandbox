/*
 * point.cpp
 *
 *  Created on: May 26, 2010
 *      Author: regis
 */

#include <math.h>
#include <GL/gl.h>
#include "point.hpp"


/****************************************************************************/
CPoint::CPoint (float NewX, float NewY, float NewZ, float NewW) : x(NewX), y(NewY), z(NewZ), w(NewW)
{
  //cout << *this << endl;
  //cout << ++count << endl;
}

/****************************************************************************/
CPoint::CPoint (float * XYZW)
{
  x = XYZW[0];
  y = XYZW[1];
  z = XYZW[2];
  w = XYZW[3];
  //cout << ++count << endl;
}

/****************************************************************************/
CPoint::CPoint (const CPoint & a) : x(a.x), y(a.y), z(a.z), w(a.w)
{
  //cout << ++count << endl;
}

/****************************************************************************/
CPoint::CPoint (const CPoint * a) : x(a->x), y(a->y), z(a->z), w(a->w)
{
  //cout << *this << endl;
}

/****************************************************************************/
CPoint::~CPoint ()
{
  //cout << --count << endl;
}

/****************************************************************************/
void CPoint::SetXYZ (float NewX, float NewY, float NewZ)
{
  x = NewX;
  y = NewY;
  z = NewZ;
}

/****************************************************************************/
void CPoint::SetXYZW (float NewX, float NewY, float NewZ, float NewW)
{
  x = NewX;
  y = NewY;
  z = NewZ;
  w = NewW;
}

/****************************************************************************/
void CPoint::SetX (float NewX)
{
  x = NewX;
}

/****************************************************************************/
void CPoint::SetY (float NewY)
{
  y = NewY;
}

/****************************************************************************/
void CPoint::SetZ (float NewZ)
{
  z = NewZ;
}

/****************************************************************************/
void CPoint::SetW (float NewW)
{
  w = NewW;
}

/****************************************************************************/
float CPoint::X ()
{
  return x;
}

/****************************************************************************/
float CPoint::Y ()
{
  return y;
}

/****************************************************************************/
float CPoint::Z ()
{
  return z;
}

/****************************************************************************/
float CPoint::W ()
{
  return w;
}

/****************************************************************************/
float * CPoint::xyzwArray ()
{
  return xyzw;
}

/****************************************************************************/
void CPoint::gl ()
{
  glVertex3fv (xyzw);
}

/****************************************************************************/
void CPoint::Normalize ()
{
  /* on calcul la norme du vecteur pour normaliser ... */
  float Norme = sqrtf ((x * x) + (y * y) + (z * z));

  x /= Norme;
  y /= Norme;
  z /= Norme;
  w = 1.0;
}

/****************************************************************************/
CPoint operator+ (const CPoint & a, const CPoint & b)
{
  CPoint tmp;
  tmp.x = (a.x + b.x);
  tmp.y = (a.y + b.y);
  tmp.z = (a.z + b.z);
  tmp.w = (a.w + b.w);

  return tmp;
}

/****************************************************************************/
CPoint operator- (const CPoint & a, const CPoint & b)
{
  CPoint tmp;
  tmp.x = (a.x - b.x);
  tmp.y = (a.y - b.y);
  tmp.z = (a.z - b.z);
  tmp.w = (a.w - b.w);

  return tmp;
}

/****************************************************************************/
CPoint operator% (const CPoint & a, const CPoint & b)
{
  CPoint tmp;
  tmp.x = (a.x * b.x);
  tmp.y = (a.y * b.y);
  tmp.z = (a.z * b.z);
  tmp.w = (a.w * b.w);

  return tmp;
}

/****************************************************************************/
CPoint operator/ (const CPoint & a, const CPoint & b)
{
  CPoint tmp;
  tmp.x = (a.x / b.x);
  tmp.y = (a.y / b.y);
  tmp.z = (a.z / b.z);
  tmp.w = (a.w / b.w);

  return tmp;
}

/****************************************************************************/
CPoint operator^ (const CPoint & a, const CPoint & b)
{
  CPoint tmp;
  tmp.x = (a.y * b.z) - (a.z * b.y);
  tmp.y = (a.z * b.x) - (a.x * b.z);
  tmp.z = (a.x * b.y) - (a.y * b.x);
  tmp.w = 1.0;

  return tmp;
}

/****************************************************************************/
bool operator== (const CPoint & a, const CPoint & b)
{
  return ( a.x == b.x && a.y == b.y && a.z == b.z && a.w == a.w );
}

/****************************************************************************/
bool operator!= (const CPoint & a, const CPoint & b)
{
  return ( a.x != b.x || a.y != b.y || a.z != b.z || a.w != a.w );
}

/****************************************************************************/
float operator* (const CPoint & a, const CPoint & b)
{
  float tmp = 0.0;
  tmp += (a.x * b.x);
  tmp += (a.y * b.y);
  tmp += (a.z * b.z);
  //tmp += (a.w * b.w);

  return tmp;
}

/****************************************************************************/
CPoint operator+ (const CPoint & a, const float & b)
{
  CPoint tmp;
  tmp.x = (a.x + b);
  tmp.y = (a.y + b);
  tmp.z = (a.z + b);
  tmp.w = (a.w + b);

  return tmp;
}

/****************************************************************************/
CPoint operator- (const CPoint & a, const float & b)
{
  CPoint tmp;
  tmp.x = (a.x - b);
  tmp.y = (a.y - b);
  tmp.z = (a.z - b);
  tmp.w = (a.w - b);

  return tmp;
}

/****************************************************************************/
CPoint operator* (const CPoint & a, const float & b)
{
  CPoint tmp;
  tmp.x = a.x * b;
  tmp.y = a.y * b;
  tmp.z = a.z * b;
  tmp.w = a.w * b;

  return tmp;
}

/****************************************************************************/
CPoint operator/ (const CPoint & a, const float & b)
{
  CPoint tmp;
  tmp.x = a.x / b;
  tmp.y = a.y / b;
  tmp.z = a.z / b;
  tmp.w = a.w / b;

  return tmp;
}

/****************************************************************************/
CPoint & CPoint::operator+= (const float & a)
{
  (*this) = (*this)+a;
  return (*this);
}

/****************************************************************************/
CPoint & CPoint::operator-= (const float & a)
{
  (*this) = (*this)-a;
  return (*this);
}

/****************************************************************************/
CPoint & CPoint::operator*= (const float & a)
{
  (*this) = (*this)*a;
  return (*this);
}

/****************************************************************************/
CPoint & CPoint::operator/= (const float & a)
{
  (*this) = (*this)/(float)a;
  return (*this);
}

/****************************************************************************/
CPoint & CPoint::operator= (const float & a)
{
  x = a;
  y = a;
  z = a;
  w = a;
  return (*this);
}

/****************************************************************************/
CPoint & CPoint::operator+= (const CPoint & a)
{
  (*this) = (*this)+a;
  return (*this);
}

/****************************************************************************/
CPoint & CPoint::operator-= (const CPoint & a)
{
  (*this) = (*this)-a;
  return (*this);
}

/****************************************************************************/
CPoint & CPoint::operator%= (const CPoint & a)
{
  (*this) = (*this)%a;
  return (*this);
}

/****************************************************************************/
CPoint & CPoint::operator/= (const CPoint & a)
{
  (*this) = (*this)/a;
  return (*this);
}

/****************************************************************************/
CPoint & CPoint::operator^= (const CPoint & a)
{
  (*this) = (*this)^a;
  return (*this);
}

/****************************************************************************/
CPoint & CPoint::operator= (const CPoint & a)
{
  x = a.x;
  y = a.y;
  z = a.z;
  w = a.w;
  return (*this);
}


/****************************************************************************/
float & CPoint::operator[] (size_t pos)
{
  if (pos >=0 && pos <=3)
    return xyzw[pos];
  else
    throw "wrong index in CPoint::operator[]";
}

/****************************************************************************/
ostream& operator<<(ostream& out, const CPoint & pt) // output
{
    out << "(" << pt.x << ", " << pt.y << ", " << pt.z << ", " << pt.w << ")";
    return out;
}
