/*
 * tools3d.cpp
 *
 *  Created on: May 28, 2010
 *      Author: regis
 */

#include <math.h>
#include "tools3d.hpp"

/****************************************************************************/
float * CalculateNormal (CPoint & Point1, CPoint & Point2, CPoint & Point3)
{
  static float Normale[3];
  float x1, x2, y1, y2, z1, z2;
  float Norme;
  CPoint Vecteur1;
  CPoint Vecteur2;

  Vecteur1.SetXYZ(Point2.X()-Point1.X(),
                  Point2.Y()-Point1.Y(),
                  Point2.Z()-Point1.Z());

  Vecteur2.SetXYZ(Point3.X()-Point1.X(),
                  Point3.Y()-Point1.Y(),
                  Point3.Z()-Point1.Z());

  x1 = Vecteur1.X();
  y1 = Vecteur1.Y();
  z1 = Vecteur1.Z();
  x2 = Vecteur2.X();
  y2 = Vecteur2.Y();
  z2 = Vecteur2.Z();

  Normale[0] = (y1 * z2) - (z1 * y2);
  Normale[1] = (z1 * x2) - (x1 * z2);
  Normale[2] = (x1 * y2) - (y1 * x2);

  /* on calcul la norme du vecteur pour normaliser ... */
  Norme = sqrtf ((Normale[0] * Normale[0]) + (Normale[1] * Normale[1]) + (Normale[2] * Normale[2]));

  Normale[0] /= Norme;
  Normale[1] /= Norme;
  Normale[2] /= Norme;

  return Normale;
}

/****************************************************************************/
CPoint * Intersect2DSegments (CPoint & pt1, CPoint & pt2, CPoint & pt3, CPoint & pt4)
{
  CPoint * result = NULL;

  /*
            (Ay-Cy)(Dx-Cx)-(Ax-Cx)(Dy-Cy)
        r = -----------------------------  (eqn 1)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

            (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
        s = -----------------------------  (eqn 2)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

   *
   */

  float r1 = ((pt1.Y()-pt3.Y()) * (pt4.X()-pt3.X()) - (pt1.X()-pt3.X()) * (pt4.Y()-pt3.Y()));
  float r2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));

  float s1 = ((pt1.Y()-pt3.Y()) * (pt2.X()-pt1.X()) - (pt1.X()-pt3.X()) * (pt2.Y()-pt1.Y()));
  float s2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));

  float r = -1;
  float s = -1;

  //cout << " r1: " << r1 << " r2: " << r2 << " s1: " << s1 << " s2: " << s2 << endl;

  if (r2 != 0)
    r = r1 / r2;
  else
    r = 0;

  /*
  if (r1 == 0) // parallel vectors
  {
    r = 0;
    if (r2 == 0) // coincident vectors
    {
      result = new CPoint ((pt1.X()+pt2.X())/2.0, (pt1.Y()+pt2.Y())/2.0, (pt1.Z()+pt2.Z())/2.0);
    }
  }
  */

  if (s2 != 0)
    s = s1 / s2;
  else
    s = 0;

  //cout << " r: " << r  << " s: " << s << endl;
  /*
  If 0<=r<=1 & 0<=s<=1, intersection exists
      r<0 or r>1 or s<0 or s>1 line segments do not intersect

  Px=Ax+r(Bx-Ax)
  Py=Ay+r(By-Ay)
   */

  if (r>=0 && r<=1 && s>=0 && s<=1)
  {
    result = new CPoint (pt1.X() + r * (pt2.X()-pt1.X()), \
                         pt1.Y() + r * (pt2.Y()-pt1.Y()), \
                         pt1.Z() + r * (pt2.Z()-pt1.Z()), \
                         1.0);
  }

  return result;
}

/****************************************************************************/
CPoint * Intersect3DSegments (CPoint & pt1, CPoint & pt2, CPoint & pt3, CPoint & pt4)
{
  CPoint * result = NULL;

  /*
            (Ay-Cy)(Dx-Cx)-(Ax-Cx)(Dy-Cy)
        r = -----------------------------  (eqn 1)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

            (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
        s = -----------------------------  (eqn 2)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

   *
   */

  float rxy1 = ((pt1.Y()-pt3.Y()) * (pt4.X()-pt3.X()) - (pt1.X()-pt3.X()) * (pt4.Y()-pt3.Y()));
  float rxy2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));
  float sxy1 = ((pt1.Y()-pt3.Y()) * (pt2.X()-pt1.X()) - (pt1.X()-pt3.X()) * (pt2.Y()-pt1.Y()));
  float sxy2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));

  float rxz1 = ((pt1.Z()-pt3.Z()) * (pt4.X()-pt3.X()) - (pt1.X()-pt3.X()) * (pt4.Z()-pt3.Z()));
  float rxz2 = ((pt2.X()-pt1.X()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.X()-pt3.X()));
  float sxz1 = ((pt1.Z()-pt3.Z()) * (pt2.X()-pt1.X()) - (pt1.X()-pt3.X()) * (pt2.Z()-pt1.Z()));
  float sxz2 = ((pt2.X()-pt1.X()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.X()-pt3.X()));

  float ryz1 = ((pt1.Z()-pt3.Z()) * (pt4.Y()-pt3.Y()) - (pt1.Y()-pt3.Y()) * (pt4.Z()-pt3.Z()));
  float ryz2 = ((pt2.Y()-pt1.Y()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.Y()-pt3.Y()));
  float syz1 = ((pt1.Z()-pt3.Z()) * (pt2.Y()-pt1.Y()) - (pt1.Y()-pt3.Y()) * (pt2.Z()-pt1.Z()));
  float syz2 = ((pt2.Y()-pt1.Y()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.Y()-pt3.Y()));

  float rxy = -1;
  float sxy = -1;

  float rxz = -1;
  float sxz = -1;

  float ryz = -1;
  float syz = -1;

  //cout << "rxy1: " << rxy1 << " rxy2: " << rxy2 << " sxy1: " << sxy1 << " sxy2: " << sxy2 << endl;
  //cout << "rxz1: " << rxz1 << " rxz2: " << rxz2 << " sxz1: " << sxz1 << " sxz2: " << sxz2 << endl;
  //cout << "ryz1: " << ryz1 << " ryz2: " << ryz2 << " syz1: " << syz1 << " syz2: " << syz2 << endl;

  if (rxy2 != 0)
    rxy = rxy1 / rxy2;
  else
    rxy = 0;

  if (sxy2 != 0)
    sxy = sxy1 / sxy2;
  else
    sxy = 0;


  if (rxz2 != 0)
    rxz = rxz1 / rxz2;
  else
    rxz = 0;

  if (sxz2 != 0)
    sxz = sxz1 / sxz2;
  else
    sxz = 0;


  if (ryz2 != 0)
    ryz = ryz1 / ryz2;
  else
    ryz = 0;

  if (syz2 != 0)
    syz = syz1 / syz2;
  else
    syz = 0;

  //cout << "rxy: " << rxy  << " sxy: " << sxy << endl;
  //cout << "rxz: " << rxz  << " sxz: " << sxz << endl;
  //cout << "ryz: " << ryz  << " syz: " << syz << endl;

  float r = -1;
  float s = -1;

  if (rxy2 == 0)
  {
    if (rxz2 == 0)
    {
      if (ryz2 == 0)
      {
        result = NULL;
      }
      else
      {
        r = ryz;
        s = syz;
      }
    }
    else
    {
      r = rxz;
      s = sxz;
    }
  }
  else
  {
    r = rxy;
    s = sxy;
  }

  //cout << "r: " << r  << " s: " << s << endl;

  if (r>=0 && r<=1 && s>=0 && s<=1)
  {
    CPoint pt12 = CPoint (pt1.X() + r * (pt2.X()-pt1.X()), \
                          pt1.Y() + r * (pt2.Y()-pt1.Y()), \
                          pt1.Z() + r * (pt2.Z()-pt1.Z()), \
                          pt1.W() + r * (pt2.W()-pt1.W()) );

    CPoint pt34 = CPoint (pt3 + ((pt4-pt3)*s));

    if ( pt12 == pt34)
    {
      result = new CPoint (pt12);

/*

      cout << pt12.X() << " ";
      cout << pt12.Y() << " ";
      cout << pt12.Z() << " ";
      cout << pt12.W() << endl;

      cout << pt34.X() << " ";
      cout << pt34.Y() << " ";
      cout << pt34.Z() << " ";
      cout << pt34.W() << endl << "-----------------" << endl;
*/
    }

  }

  return result;
}

/****************************************************************************/
CPoint * Intersect2DLines (CPoint & pt1, CPoint & pt2, CPoint & pt3, CPoint & pt4)
{
  CPoint * result = NULL;

  /*
            (Ay-Cy)(Dx-Cx)-(Ax-Cx)(Dy-Cy)
        r = -----------------------------  (eqn 1)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

            (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
        s = -----------------------------  (eqn 2)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

   *
   */

  float r1 = ((pt1.Y()-pt3.Y()) * (pt4.X()-pt3.X()) - (pt1.X()-pt3.X()) * (pt4.Y()-pt3.Y()));
  float r2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));

  float s1 = ((pt1.Y()-pt3.Y()) * (pt2.X()-pt1.X()) - (pt1.X()-pt3.X()) * (pt2.Y()-pt1.Y()));
  float s2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));

  float r = -1;
  float s = -1;

  //cout << " r1: " << r1 << " r2: " << r2 << " s1: " << s1 << " s2: " << s2 << endl;

  if (r2 != 0)
    r = r1 / r2;
  else
    r = 0;

  /*
  if (r1 == 0) // parallel vectors
  {
    r = 0;
    if (r2 == 0) // coincident vectors
    {
      result = new CPoint ((pt1.X()+pt2.X())/2.0, (pt1.Y()+pt2.Y())/2.0, (pt1.Z()+pt2.Z())/2.0);
    }
  }
  */

  if (s2 != 0)
    s = s1 / s2;
  else
    s = 0;

  //cout << " r: " << r  << " s: " << s << endl;
  /*
  If 0<=r<=1 & 0<=s<=1, intersection exists
      r<0 or r>1 or s<0 or s>1 line segments do not intersect

  Px=Ax+r(Bx-Ax)
  Py=Ay+r(By-Ay)
   */

  if (r2 != 0) //removes  "|| r1 !=0" in first part : wrong. Only the test on r2 is needed
  {
    //cout << pt1 << " ||| " << pt2 << endl;
    //cout << pt1.X() + r * (pt2.X()-pt1.X()) << " " << pt1.Y() + r * (pt2.Y()-pt1.Y()) << " " << pt1.Z() << " " << 1.0 << endl;

    result = new CPoint (pt1.X() + r * (pt2.X()-pt1.X()), pt1.Y() + r * (pt2.Y()-pt1.Y()), pt1.Z() + r * (pt2.Z()-pt1.Z()), 1.0);
  }

  return result;
}

/****************************************************************************/
CPoint * Intersect3DLines (CPoint & pt1, CPoint & pt2, CPoint & pt3, CPoint & pt4)
{
  CPoint * result = NULL;

  /*
            (Ay-Cy)(Dx-Cx)-(Ax-Cx)(Dy-Cy)
        r = -----------------------------  (eqn 1)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

            (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
        s = -----------------------------  (eqn 2)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

   *
   */

  float rxy1 = ((pt1.Y()-pt3.Y()) * (pt4.X()-pt3.X()) - (pt1.X()-pt3.X()) * (pt4.Y()-pt3.Y()));
  float rxy2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));
  float sxy1 = ((pt1.Y()-pt3.Y()) * (pt2.X()-pt1.X()) - (pt1.X()-pt3.X()) * (pt2.Y()-pt1.Y()));
  float sxy2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));

  float rxz1 = ((pt1.Z()-pt3.Z()) * (pt4.X()-pt3.X()) - (pt1.X()-pt3.X()) * (pt4.Z()-pt3.Z()));
  float rxz2 = ((pt2.X()-pt1.X()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.X()-pt3.X()));
  float sxz1 = ((pt1.Z()-pt3.Z()) * (pt2.X()-pt1.X()) - (pt1.X()-pt3.X()) * (pt2.Z()-pt1.Z()));
  float sxz2 = ((pt2.X()-pt1.X()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.X()-pt3.X()));

  float ryz1 = ((pt1.Z()-pt3.Z()) * (pt4.Y()-pt3.Y()) - (pt1.Y()-pt3.Y()) * (pt4.Z()-pt3.Z()));
  float ryz2 = ((pt2.Y()-pt1.Y()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.Y()-pt3.Y()));
  float syz1 = ((pt1.Z()-pt3.Z()) * (pt2.Y()-pt1.Y()) - (pt1.Y()-pt3.Y()) * (pt2.Z()-pt1.Z()));
  float syz2 = ((pt2.Y()-pt1.Y()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.Y()-pt3.Y()));

  float rxy = -1;
  float sxy = -1;

  float rxz = -1;
  float sxz = -1;

  float ryz = -1;
  float syz = -1;

  //cout << "rxy1: " << rxy1 << " rxy2: " << rxy2 << " sxy1: " << sxy1 << " sxy2: " << sxy2 << endl;
  //cout << "rxz1: " << rxz1 << " rxz2: " << rxz2 << " sxz1: " << sxz1 << " sxz2: " << sxz2 << endl;
  //cout << "ryz1: " << ryz1 << " ryz2: " << ryz2 << " syz1: " << syz1 << " syz2: " << syz2 << endl;

  if (rxy2 != 0)
    rxy = rxy1 / rxy2;
  else
    rxy = 0;

  if (sxy2 != 0)
    sxy = sxy1 / sxy2;
  else
    sxy = 0;


  if (rxz2 != 0)
    rxz = rxz1 / rxz2;
  else
    rxz = 0;

  if (sxz2 != 0)
    sxz = sxz1 / sxz2;
  else
    sxz = 0;


  if (ryz2 != 0)
    ryz = ryz1 / ryz2;
  else
    ryz = 0;

  if (syz2 != 0)
    syz = syz1 / syz2;
  else
    syz = 0;

  //cout << "rxy: " << rxy  << " sxy: " << sxy << endl;
  //cout << "rxz: " << rxz  << " sxz: " << sxz << endl;
  //cout << "ryz: " << ryz  << " syz: " << syz << endl;

  float r = -1;
  float s = -1;

  float r1 = 0;
  float r2 = 0;

  if (rxy2 == 0.0)
  {
    if (rxz2 == 0.0)
    {
      if (ryz2 == 0.0)
      {
        result = NULL;
      }
      else
      {
        r = ryz;
        r1 = ryz1;
        r2 = ryz2;

        s = syz;
      }
    }
    else
    {
      r = rxz;
      r1 = rxz1;
      r2 = rxz2;

      s = sxz;
    }
  }
  else
  {
    r = rxy;
    r1 = rxy1;
    r2 = rxy2;

    s = sxy;
  }

  //cout << "r: " << r  << " s: " << s << endl;
  //cout << "r1: " << r1  << " r2: " << r2 << " r1/r2: " << r1/r2 << endl;

  if (r2 != 0) //removes  "|| r1 !=0" in first part : wrong. Only the test on r2 is needed
  {
    CPoint pt12 = CPoint (pt1.X() + r * (pt2.X()-pt1.X()), \
                          pt1.Y() + r * (pt2.Y()-pt1.Y()), \
                          pt1.Z() + r * (pt2.Z()-pt1.Z()), \
                          pt1.W() + r * (pt2.W()-pt1.W()) );

    CPoint pt34 = CPoint (pt3 + ((pt4-pt3)*s));

    //cout << pt12 << " " << pt34 << endl;

    if ( pt12 == pt34)
    {
      result = new CPoint (pt12);

/*

      cout << pt12.X() << " ";
      cout << pt12.Y() << " ";
      cout << pt12.Z() << " ";
      cout << pt12.W() << endl;

      cout << pt34.X() << " ";
      cout << pt34.Y() << " ";
      cout << pt34.Z() << " ";
      cout << pt34.W() << endl << "-----------------" << endl;
*/
    }

  }

  return result;
}

/****************************************************************************/
CPoint * Intersect2DLineToSegment (CPoint & pt3, CPoint & pt4, CPoint & pt1, CPoint & pt2)
{
  CPoint * result = NULL;

/*
  cout << pt3.X() << " " << pt3.Y() << endl;
  cout << pt4.X() << " " << pt4.Y() << endl;
  cout << pt1.X() << " " << pt1.Y() << endl;
  cout << pt2.X() << " " << pt2.Y()  << endl << endl;
*/

  /*
            (Ay-Cy)(Dx-Cx)-(Ax-Cx)(Dy-Cy)
        r = -----------------------------  (eqn 1)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

            (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
        s = -----------------------------  (eqn 2)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

   */

  float r1 = ((pt1.Y()-pt3.Y()) * (pt4.X()-pt3.X()) - (pt1.X()-pt3.X()) * (pt4.Y()-pt3.Y()));
  float r2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));

  float s1 = ((pt1.Y()-pt3.Y()) * (pt2.X()-pt1.X()) - (pt1.X()-pt3.X()) * (pt2.Y()-pt1.Y()));
  float s2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));

  float r = -1;
  float s = -1;

  //cout << " r1: " << r1 << " r2: " << r2 << " s1: " << s1 << " s2: " << s2 << endl;

  if (r2 != 0)
    r = r1 / r2;
  else
    r = 0;

  /*
  if (r1 == 0) // parallel vectors
  {
    r = 0;
    if (r2 == 0) // coincident vectors
    {
      result = new CPoint ((pt1.X()+pt2.X())/2.0, (pt1.Y()+pt2.Y())/2.0, (pt1.Z()+pt2.Z())/2.0);
    }
  }
  */

  if (s2 != 0)
    s = s1 / s2;
  else
    s = 0;

  //cout << " r: " << r  << " s: " << s << endl;
  /*
  If 0<=r<=1 & 0<=s<=1, intersection exists
      r<0 or r>1 or s<0 or s>1 line segments do not intersect

  Px=Ax+r(Bx-Ax)
  Py=Ay+r(By-Ay)
   */



  if ((r2 != 0) && (r>=0 && r<=1)) //removes  "|| r1 !=0" in first part : wrong. Only the test on r2 is needed
  {
    //cout << pt1 << " ||| " << pt2 << endl;
    //cout << pt1.X() + r * (pt2.X()-pt1.X()) << " " << pt1.Y() + r * (pt2.Y()-pt1.Y()) << " " << pt1.Z() << " " << 1.0 << endl;

    result = new CPoint (pt1.X() + r * (pt2.X()-pt1.X()), pt1.Y() + r * (pt2.Y()-pt1.Y()), pt1.Z() + r * (pt2.Z()-pt1.Z()), 1.0);
  }

  return result;
}

/****************************************************************************/
CPoint * Intersect3DLineToSegment (CPoint & pt3, CPoint & pt4, CPoint & pt1, CPoint & pt2)
{
  CPoint * result = NULL;

  /*
            (Ay-Cy)(Dx-Cx)-(Ax-Cx)(Dy-Cy)
        r = -----------------------------  (eqn 1)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)

            (Ay-Cy)(Bx-Ax)-(Ax-Cx)(By-Ay)
        s = -----------------------------  (eqn 2)
            (Bx-Ax)(Dy-Cy)-(By-Ay)(Dx-Cx)
   */

  float rxy1 = ((pt1.Y()-pt3.Y()) * (pt4.X()-pt3.X()) - (pt1.X()-pt3.X()) * (pt4.Y()-pt3.Y()));
  float rxy2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));
  float sxy1 = ((pt1.Y()-pt3.Y()) * (pt2.X()-pt1.X()) - (pt1.X()-pt3.X()) * (pt2.Y()-pt1.Y()));
  float sxy2 = ((pt2.X()-pt1.X()) * (pt4.Y()-pt3.Y()) - (pt2.Y()-pt1.Y()) * (pt4.X()-pt3.X()));

  float rxz1 = ((pt1.Z()-pt3.Z()) * (pt4.X()-pt3.X()) - (pt1.X()-pt3.X()) * (pt4.Z()-pt3.Z()));
  float rxz2 = ((pt2.X()-pt1.X()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.X()-pt3.X()));
  float sxz1 = ((pt1.Z()-pt3.Z()) * (pt2.X()-pt1.X()) - (pt1.X()-pt3.X()) * (pt2.Z()-pt1.Z()));
  float sxz2 = ((pt2.X()-pt1.X()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.X()-pt3.X()));

  float ryz1 = ((pt1.Z()-pt3.Z()) * (pt4.Y()-pt3.Y()) - (pt1.Y()-pt3.Y()) * (pt4.Z()-pt3.Z()));
  float ryz2 = ((pt2.Y()-pt1.Y()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.Y()-pt3.Y()));
  float syz1 = ((pt1.Z()-pt3.Z()) * (pt2.Y()-pt1.Y()) - (pt1.Y()-pt3.Y()) * (pt2.Z()-pt1.Z()));
  float syz2 = ((pt2.Y()-pt1.Y()) * (pt4.Z()-pt3.Z()) - (pt2.Z()-pt1.Z()) * (pt4.Y()-pt3.Y()));

  float rxy = -1;
  float sxy = -1;

  float rxz = -1;
  float sxz = -1;

  float ryz = -1;
  float syz = -1;

  //cout << "rxy1: " << rxy1 << " rxy2: " << rxy2 << " sxy1: " << sxy1 << " sxy2: " << sxy2 << endl;
  //cout << "rxz1: " << rxz1 << " rxz2: " << rxz2 << " sxz1: " << sxz1 << " sxz2: " << sxz2 << endl;
  //cout << "ryz1: " << ryz1 << " ryz2: " << ryz2 << " syz1: " << syz1 << " syz2: " << syz2 << endl;

  if (rxy2 != 0)
    rxy = rxy1 / rxy2;
  else
    rxy = 0;

  if (sxy2 != 0)
    sxy = sxy1 / sxy2;
  else
    sxy = 0;


  if (rxz2 != 0)
    rxz = rxz1 / rxz2;
  else
    rxz = 0;

  if (sxz2 != 0)
    sxz = sxz1 / sxz2;
  else
    sxz = 0;


  if (ryz2 != 0)
    ryz = ryz1 / ryz2;
  else
    ryz = 0;

  if (syz2 != 0)
    syz = syz1 / syz2;
  else
    syz = 0;

  //cout << "rxy: " << rxy  << " sxy: " << sxy << endl;
  //cout << "rxz: " << rxz  << " sxz: " << sxz << endl;
  //cout << "ryz: " << ryz  << " syz: " << syz << endl;

  float r = -1;
  float s = -1;

  float r1 = 0;
  float r2 = 0;

  if (rxy2 == 0.0)
  {
    if (rxz2 == 0.0)
    {
      if (ryz2 == 0.0)
      {
        result = NULL;
      }
      else
      {
        r = ryz;
        r1 = ryz1;
        r2 = ryz2;

        s = syz;
      }
    }
    else
    {
      r = rxz;
      r1 = rxz1;
      r2 = rxz2;

      s = sxz;
    }
  }
  else
  {
    r = rxy;
    r1 = rxy1;
    r2 = rxy2;

    s = sxy;
  }

  //cout << "r: " << r  << " s: " << s << endl;
  //cout << "r1: " << r1  << " r2: " << r2 << " r1/r2: " << r1/r2 << endl;

  if ((r2 != 0) && (r>=0 && r<=1)) //removes  "|| r1 !=0" in first part : wrong. Only the test on r2 is needed
  {
    CPoint pt12 = CPoint (pt1.X() + r * (pt2.X()-pt1.X()), \
                          pt1.Y() + r * (pt2.Y()-pt1.Y()), \
                          pt1.Z() + r * (pt2.Z()-pt1.Z()), \
                          pt1.W() + r * (pt2.W()-pt1.W()) );

    CPoint pt34 = CPoint (pt3 + ((pt4-pt3)*s));

    //cout << pt12 << " " << pt34 << endl;

    if ( pt12 == pt34)
    {
      result = new CPoint (pt12);

/*

      cout << pt12.X() << " ";
      cout << pt12.Y() << " ";
      cout << pt12.Z() << " ";
      cout << pt12.W() << endl;

      cout << pt34.X() << " ";
      cout << pt34.Y() << " ";
      cout << pt34.Z() << " ";
      cout << pt34.W() << endl << "-----------------" << endl;
*/
    }
  }

  return result;
}

/****************************************************************************/
float VectorLength (CPoint & vect)
{
  // square root ( x²+y²+z² )
  return sqrtf ((vect[0] * vect[0]) + (vect[1] * vect[1]) + (vect[2] * vect[2]));
}

/****************************************************************************/
float DistancePointToPoint (CPoint & pt1, CPoint pt2)
{
  CPoint tmp = pt2-pt1;
  return VectorLength (tmp);
}

#define SEED 14.0

float Noise2D(float x,float y)
{
  float a = acos(cos(((x+SEED*9939.0134)*(x+546.1976)+1)/(y*(y+48.9995)+149.7913)) + sin(x+y/71.0013))/PI;
  float b = sin(a*10000+SEED) + 1;
  return b*.5;
}