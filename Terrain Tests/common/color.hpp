/***************************************************************************
 *            color.h
 *
 *  Sat May 26 15:34:02 2010
 *  Copyright  2006  Regis Martinez
 *  regis.martinez@worldonline.fr
 ****************************************************************************/


class CColor
{
    float rgba[4];

  public:
    CColor (float r = 1.0, float g = 1.0, float b = 1.0, float a = 1.0);
    virtual ~CColor ();

    float * getColorArray ();
    float getRed ();
    float getGreen ();
    float getBlue ();
    float getAlpha ();

    void setRGBA (float, float, float, float);
    void setRed (float);
    void setGreen (float);
    void setBlue (float);
    void setAlpha (float);
};
