/***************************************************************************
 *            camera.cpp
 *
 *  Sat May 13 19:58:21 2006
 *  Copyright  2006  Regis Martinez
 *  regis.martinez@worldonline.fr
 ****************************************************************************/

#include <GL/glut.h>
#include <math.h>
#include "camera.hpp"
#include "tools3d.hpp" // needed for PI

/****************************************************************************/
CCamera::CCamera ()
{
  m_Orientation = 0.0;
  m_Pitch = 0.0;
  m_RotationStep = 0.08;
  ConvertDegRad ();
  m_Left = -2.0;
  m_Right = 2.0;
  m_Top = 1.5;
  m_Bottom = -1.5;
  m_Perspective = true;
}

/****************************************************************************/
CCamera::CCamera (float X, float Y, float Z, float NewOrientation, float NewInclinaison)
{ 
  m_Position.SetX(X);
  m_Position.SetY(Y);
  m_Position.SetZ(Z);
  m_Orientation = NewOrientation;
  m_Pitch = NewInclinaison;
  m_RotationStep = 0.08;
  ConvertDegRad ();
  m_Left = -2.0;
  m_Right = 2.0;
  m_Top = 1.5;
  m_Bottom = -1.5;
  m_Perspective = true;
}

/****************************************************************************/
void CCamera::InitCamera(double fovy, double aspect, double zNear,double zFar)
{
  m_FOV = fovy;
  m_Aspect = aspect;
  m_Near = zNear;
  m_Far = zFar;
  glMatrixMode (GL_MODELVIEW);
}

/****************************************************************************/
CCamera::~CCamera()
{
}

/****************************************************************************/
float CCamera::X ()
{ return m_Position.X(); }

/****************************************************************************/
float CCamera::Y () {return m_Position.Y();}

/****************************************************************************/
float CCamera::Z () {return m_Position.Z();}

/****************************************************************************/
float CCamera::GetOrientation () { return m_Orientation; }

/****************************************************************************/
float CCamera::GetPitch () { return m_Pitch; }

/****************************************************************************/
double CCamera::GetFOV () { return m_FOV; }

/****************************************************************************/
double CCamera::GetAspect () { return m_Aspect; }

/****************************************************************************/
double CCamera::GetNear () { return m_Near; }

/****************************************************************************/
double CCamera::GetFar () { return m_Far; }

/****************************************************************************/
double CCamera::GetRight () { return m_Right; }

/****************************************************************************/
double CCamera::GetLeft () { return m_Left; }

/****************************************************************************/
double CCamera::GetBottom () { return m_Bottom; }

/****************************************************************************/
double CCamera::GetTop () { return m_Top; }

/****************************************************************************/
float CCamera::GetRotationStep () { return m_RotationStep; }

/****************************************************************************/
bool CCamera::GetPerspective () { return m_Perspective; }
    

/****************************************************************************/
void CCamera::SetXYZ (float NewX, float NewY, float NewZ)
{
  m_Position.SetX(NewX);
  m_Position.SetY(NewY);
  m_Position.SetZ(NewZ);
}

/****************************************************************************/
void CCamera::SetX (float NewX) {m_Position.SetX(NewX);}

/****************************************************************************/
void CCamera::SetY (float NewY) {m_Position.SetY(NewY);}

/****************************************************************************/
void CCamera::SetZ (float NewZ) {m_Position.SetZ(NewZ);}

/****************************************************************************/
void CCamera::SetOrientation (float NewOrientation) { m_Orientation = NewOrientation; }

/****************************************************************************/
void CCamera::SetPitch (float NewPitch) { m_Pitch = NewPitch; }

/****************************************************************************/
void CCamera::SetFOV (double NewAV) { m_FOV = NewAV; }

/****************************************************************************/
void CCamera::SetAspect (double NewAspect) { m_Aspect = NewAspect; }

/****************************************************************************/
void CCamera::SetNear (double NewNear) { m_Near = NewNear; }

/****************************************************************************/
void CCamera::SetFar (double NewFar) { m_Far = NewFar; }

/****************************************************************************/
void CCamera::SetRight (double NewRight) { m_Right = NewRight; }

/****************************************************************************/
void CCamera::SetLeft (double NewLeft) { m_Left = NewLeft; }

/****************************************************************************/
void CCamera::SetBottom (double NewBottom) { m_Bottom = NewBottom; }

/****************************************************************************/
void CCamera::SetTop (double NewTop) { m_Top = NewTop; }

/****************************************************************************/
void CCamera::SetRotationStep (float NewRS ) { m_RotationStep = NewRS; }

/****************************************************************************/
void CCamera::SetPerspective (bool val) { m_Perspective = val; }

/****************************************************************************/
void CCamera::MoveRight (float MoveStep)
{
  m_Position.SetZ(m_Position.Z() + (MoveStep * sin(m_RadOrientation)));
  m_Position.SetX(m_Position.X() + (MoveStep * cos(m_RadOrientation)));
}

/****************************************************************************/    
void CCamera::MoveLeft (float MoveStep)
{
  m_Position.SetZ(m_Position.Z() - (MoveStep * sin(m_RadOrientation)));
  m_Position.SetX(m_Position.X() - (MoveStep * cos(m_RadOrientation)));
}

/****************************************************************************/    
void CCamera::MoveAhead (float MoveStep)
{
  m_Position.SetZ(m_Position.Z() - (MoveStep * cos(m_RadOrientation) * cos(m_RadPitch)));
  m_Position.SetX(m_Position.X() - (MoveStep * (-sin(m_RadOrientation)) * cos(m_RadPitch)));
  m_Position.SetY(m_Position.Y() - (MoveStep * sin(m_RadPitch)));
}

/****************************************************************************/    
void CCamera::MoveBack (float MoveStep)
{
  m_Position.SetZ(m_Position.Z() + (MoveStep * cos(m_RadOrientation) * cos(m_RadPitch)));
  m_Position.SetX(m_Position.X() + (MoveStep * (-sin(m_RadOrientation)) * cos(m_RadPitch)));
  m_Position.SetY(m_Position.Y() + (MoveStep * sin(m_RadPitch)));
}

/****************************************************************************/    
void CCamera::MoveUp (float MoveStep)
{ m_Position.SetY(m_Position.Y() + MoveStep); }

/****************************************************************************/    
void CCamera::MoveDown (float MoveStep)
{ m_Position.SetY(m_Position.Y() - MoveStep); }

/****************************************************************************/
void CCamera::YawLeftRight (int Rotation)
{
  m_Orientation -= Rotation * m_RotationStep;
  if (m_Orientation >= 360)
    m_Orientation -= 360;
  if (m_Orientation < 0)
    m_Orientation += 360;
  
  ConvertDegRad();
}

/****************************************************************************/    
void CCamera::Pitch(int Rotation)
{
  m_Pitch -= Rotation * m_RotationStep;
  if (m_Pitch >= 90)
    m_Pitch = 90;

  if (m_Pitch < -90)
    m_Pitch = -90;
    
  ConvertDegRad();
}

/****************************************************************************/    
void CCamera::ConvertDegRad ()
{
  m_RadPitch = (((double)m_Pitch/360.0)*(2.0*PI));
  m_RadOrientation = (((double)m_Orientation/360.0)*(2.0*PI));
}

/****************************************************************************/    
void CCamera::ApplyProjection ()
{ 
  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  
  if (m_Perspective)
    gluPerspective(m_FOV, m_Aspect, m_Near, m_Far);
  else
    glOrtho (m_Left, m_Right, m_Bottom, m_Top, m_Near, m_Far);

  glMatrixMode (GL_MODELVIEW);

  /* Application des transformation de visualisation */
  glRotated(m_Pitch,1.0,0.0,0.0);
  glRotated(m_Orientation,0.0,1.0,0.0);
  glTranslatef(-m_Position.X(),-m_Position.Y(),-m_Position.Z());
}
