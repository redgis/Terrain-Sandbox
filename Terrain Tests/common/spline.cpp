/***************************************************************************
 *            spline.cpp
 *
 *  Sat May 13 15:34:02 2006
 *  Copyright  2006  Regis Martinez
 *  regis.martinez@worldonline.fr
 ****************************************************************************/

#include <GL/gl.h>
#include <math.h>
#include <string.h>
#include "spline.hpp"
#include "tools3d.hpp"
#include "stltools.hpp"

/****************************************************************************/
CSpline::CSpline (int discretization, CColor curveColor, CColor pointsColor) : m_Discretization(discretization), m_CurveColor(curveColor), m_PointsColor (pointsColor)
{
  m_DrawPoints = false;
  setCatmullRomMatrix (0.7);
  m_Closed = false;
  m_Continuous = false;
  m_ReachAllPoints = false;
  m_AdaptativeTension = false;
}

/****************************************************************************/
CSpline::~CSpline ()
{
  //delete points


}

/****************************************************************************/
void CSpline::setBezierMatrix ()
{
  float Mtmp[4][4] = {             // Bezier basis matrix
  {-1.0f, 3.0f,-3.0f, 1.0f},
  { 3.0f,-6.0f, 3.0f, 0.0f},
  {-3.0f, 3.0f, 0.0f, 0.0f},
  { 1.0f, 0.0f, 0.0f, 0.0f}
  };

  memcpy ((void *) m_Matrix, (void *) Mtmp, 4*4*sizeof(float));

  m_AdaptativeTension = false;
}

/****************************************************************************/
void CSpline::setCatmullRomMatrix (float S, bool AdaptativeTension)
{
  float Mtmp [4][4] = {        // Catmull-Rom basis matrix
    {    -S, 2.0f-S,       S-2.0f,    S},
    {2.0f*S, S-3.0f,  3.0f-2.0f*S,   -S},
    {    -S,   0.0f,            S, 0.0f},
    {  0.0f,   1.0f,         0.0f, 0.0f}
  };

  memcpy ((void *) m_Matrix, (void *) Mtmp, 4*4*sizeof(float));
  m_AdaptativeTension = AdaptativeTension;
}

/****************************************************************************/
void CSpline::setHermiteMatrix ()
{
  float Mtmp [4][4] = {    // Hermite basis matrix
    { 2.0f,-2.0f, 1.0f, 1.0f},
    {-3.0f, 3.0f,-2.0f,-1.0f},
    { 0.0f, 0.0f, 1.0f, 0.0f},
    { 1.0f, 0.0f, 0.0f, 0.0f}
  };

  memcpy ((void *) m_Matrix, (void *) Mtmp, 4*4*sizeof(float));
  m_AdaptativeTension = false;
}

/****************************************************************************/
void CSpline::setBSplineMatrix ()
{
  float Mtmp [4][4] = {      // B-spline basis matrix
    {-1.0f/6.0f, 1.0f/2.0f,-1.0f/2.0f, 1.0f/6.0f},
    { 1.0f/2.0f,-1.0f  , 1.0f/2.0f, 0.0f   },
    {-1.0f/2.0f, 0.0f   , 1.0f/2.0f, 0.0f   },
    { 1.0f/6.0f, 2.0f/3.0f, 1.0f/6.0f, 0.0f   }
  };

  memcpy ((void *) m_Matrix, (void *) Mtmp, 4*4*sizeof(float));
  m_AdaptativeTension = false;
}

/****************************************************************************/
void CSpline::addPoint (CPoint * newPoint)
{
  m_ControlPoints.push_back(newPoint);
}

/****************************************************************************/
void CSpline::addPoint (CPoint & newPoint)
{
  CPoint * tmpPoint = new CPoint (newPoint);
  m_ControlPoints.push_back(tmpPoint);
}

/****************************************************************************/
void CSpline::addPoint (float x, float y, float z)
{
  CPoint * tmpPoint = new CPoint (x, y, z);
  m_ControlPoints.push_back(tmpPoint);
}

/****************************************************************************/
int CSpline::getNbPoints ()
{
  return m_ControlPoints.size ();
}

/****************************************************************************/
deque<CPoint *> & CSpline::getControlPoints ()
{
  return m_ControlPoints;
}

/****************************************************************************/
int CSpline::getDiscretization ()
{
  return m_Discretization;
}

/****************************************************************************/
CColor & CSpline::getCurveColor ()
{
  return m_CurveColor;
}

/****************************************************************************/
CColor & CSpline::getPointsColor ()
{
  return m_PointsColor;
}

/****************************************************************************/
void CSpline::drawControlPoints (bool draw)
{
  m_DrawPoints = draw;
}

/****************************************************************************/
void CSpline::adaptativeTension (bool newVal)
{
  m_AdaptativeTension = newVal;
}

/****************************************************************************/
bool CSpline::ataptativeTension ()
{
  return m_AdaptativeTension;
}

/****************************************************************************/
bool CSpline::drawableCrontrolPoints ()
{
  return m_DrawPoints;
}

/****************************************************************************/
void CSpline::setCurveColor (CColor curveColor)
{
  m_CurveColor = curveColor;
}

/****************************************************************************/
void CSpline::setPointsColor (CColor pointsColor)
{
  m_PointsColor = pointsColor;
}

/****************************************************************************/
void CSpline::setDiscretization (int discret)
{
  m_Discretization = discret;
}

/****************************************************************************/
void CSpline::addControlPoints (deque<CPoint *> & ptctl)
{ 
  m_ControlPoints.assign (ptctl.begin (), ptctl.end ());
}

/****************************************************************************/
void CSpline::zeroControlPoints ()
{
  cleanStuffs(m_ControlPoints);
}

/****************************************************************************/
void CSpline::Build ()
{
  if (glIsList (m_idDrawList))
    glDeleteLists (m_idDrawList, 1);

  glNewList(m_idDrawList, GL_COMPILE);
  glPushAttrib(GL_ALL_ATTRIB_BITS);
  glPushMatrix();

    CPoint PointTmp;

    deque<CPoint*>::iterator itCurrentPoint = m_ControlPoints.begin();
    deque<CPoint*>::iterator itLastPoint = m_ControlPoints.end();

    /* Display control points */
    if (drawableCrontrolPoints())
    {
      glColor3fv (m_PointsColor.getColorArray());
      glBegin(GL_POINTS);
      while (itCurrentPoint != itLastPoint)
      {
        glVertex3f ((*itCurrentPoint)->X(), (*itCurrentPoint)->Y(), (*itCurrentPoint)->Z());
        itCurrentPoint++;
      }
      glEnd();
    }

/*
    if (getNbPoints() >= 4)
    {
      // Affichage de la courbe
      glColor3fv (m_CurveColor.getColorArray());
      glBegin(GL_LINE_STRIP);
      for (int Index = 0; Index <= getNbPoints() - 3; Index++)
      {
        float t = (float)Index/(float)(m_Discretization-1);
        PointTmp = getSplinePoint (t);
        glVertex3f (PointTmp.X(), PointTmp.Y(), PointTmp.Z());
      }
      glEnd();
    }
*/



/**/

    int N = getNbPoints ();

    // Draw the curve
    if (N >= 4)
    {
      glColor3fv (m_CurveColor.getColorArray());
      glBegin(GL_LINE_STRIP);

      for (int Index = 0; Index <= m_Discretization-1; Index++)
      {
        float t = (float)Index/(float)(m_Discretization-1);
        PointTmp = (CPoint) getSplinePoint (t);
        glColor3f(t, 1.0-t, 0);
        glVertex3f (PointTmp.X(), PointTmp.Y(), PointTmp.Z());
      }

      glEnd();
    }
/**/

  glPopMatrix();
  glPopAttrib();
  glEndList();
}

/****************************************************************************/
int CSpline::get3DIntersections (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, bool lineIntersection)
{
  CPoint tmpPoint1;
  CPoint tmpPoint2;
  CPoint * tmpIntersection;

  int N = getNbPoints ();
  int NbIntersect = 0;

  deque<CPoint *>::iterator itIntersection;
  deque<CPoint *>::iterator itLastIntersection;

  if (N >= 4)
  {
    tmpPoint1 = (CPoint) getSplinePoint (0.0);
    for (int Index = 1; Index <= m_Discretization-1; Index++)
    {
      float t = (float)Index/(float)(m_Discretization-1);
      tmpPoint2 = (CPoint) getSplinePoint (t);

      // Should (AB) be considered as a line or as a segment ?
      if (lineIntersection)
        tmpIntersection = Intersect3DLineToSegment (A, B, tmpPoint1, tmpPoint2);
      else
        tmpIntersection = Intersect3DSegments (A, B, tmpPoint1, tmpPoint2);
      if (tmpIntersection)
      {

        itIntersection = Intersections.begin ();
        itLastIntersection = Intersections.end ();
        while ((itIntersection != itLastIntersection) && (*((*itIntersection)) * A < (*tmpIntersection) * A) )
        {
          itIntersection ++;
        }
        Intersections.insert(itIntersection, tmpIntersection);
        NbIntersect++;
      }

      tmpPoint1 = tmpPoint2;
    }
  }

  return NbIntersect;
}

/****************************************************************************/
int CSpline::get2DIntersections (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, int Axis0, int Axis1, int Axis2, bool lineIntersection)
{
  CPoint Point1;
  CPoint Point2;
  CPoint * tmpIntersection;
  CPoint resultIntersection;

  CPoint tmpPoint1;
  CPoint tmpPoint2;
  CPoint tmpA;
  CPoint tmpB;

  int N = getNbPoints ();
  int NbIntersect = 0;

  deque<CPoint *>::iterator itIntersection;
  deque<CPoint *>::iterator itLastIntersection;

  if (N >= 4)
  {
    Point1 = (CPoint) getSplinePoint (0.0);
    for (int Index = 1; Index <= m_Discretization-1; Index++)
    {
      float t = (float)Index/(float)(m_Discretization-1);
      Point2 = (CPoint) getSplinePoint (t);

      // Convert points to make intersection in XY plan
      tmpPoint1[0] = Point1[Axis0];
      tmpPoint1[1] = Point1[Axis1];
      tmpPoint1[2] = Point1[Axis2];

      tmpPoint2[0] = Point2[Axis0];
      tmpPoint2[1] = Point2[Axis1];
      tmpPoint2[2] = Point2[Axis2];

      tmpA[0] = A[Axis0];
      tmpA[1] = A[Axis1];
      tmpA[2] = A[Axis2];

      tmpB[0] = B[Axis0];
      tmpB[1] = B[Axis1];
      tmpB[2] = B[Axis2];

      // Calculate intersection
      if (lineIntersection)  // Should (AB) be considered as a line or as a segment ?
        tmpIntersection = Intersect2DLineToSegment (tmpA, tmpB, tmpPoint1, tmpPoint2);
      else
        tmpIntersection = Intersect2DSegments (tmpA, tmpB, tmpPoint1, tmpPoint2);
      if (tmpIntersection)
      {
        // Reconvert coordinates of intersaction
        resultIntersection = *tmpIntersection;
        (*tmpIntersection)[Axis0] = resultIntersection[0];
        (*tmpIntersection)[Axis1] = resultIntersection[1];
        (*tmpIntersection)[Axis2] = resultIntersection[2];

        itIntersection = Intersections.begin ();
        itLastIntersection = Intersections.end ();
        while ((itIntersection != itLastIntersection) && (*((*itIntersection)) * A < (*tmpIntersection) * A) )
        {
          itIntersection ++;
        }
        Intersections.insert(itIntersection, tmpIntersection);

        NbIntersect++;
      }

      Point1 = Point2;
    }
  }

  return NbIntersect;
}

/****************************************************************************/
CPoint CSpline::getSplinePoint (float t)
{
  int n = getNbPoints ();
  int Index;

  CPoint Res;
  float matu[4];
  float matp[4];

  float i = 1.0f / (float)(n-3); // Ratio de chaque portion de courbe
  Index = (int) floor (t/i); // Indice de la portion de courbe
  float u = (t - (Index * i))/i; // u est le t sur l'ens de la courbe

  matu[0]=u*u*u;
  matu[1]=u*u;
  matu[2]=u;
  matu[3]=1;

  //cout << "Index : " << Index << " t: " << t << " i: " << i << " u:" << u << endl;
  if (!m_AdaptativeTension)
  {
    float distance = DistancePointToPoint (*(m_ControlPoints[Index+1]), *(m_ControlPoints[Index+2]));
    if (distance <= 1.0)
      setCatmullRomMatrix(0.0);
    else
      setCatmullRomMatrix(0.7);
  }

  matp[0]=m_ControlPoints[Index]->X();
  matp[1]=m_ControlPoints[Index+1]->X();
  matp[2]=m_ControlPoints[Index+2]->X();
  if (t >= 1.0)
    matp[3]=m_ControlPoints[Index+2]->X();
  else
    matp[3]=m_ControlPoints[Index+3]->X();
  Res.SetX(multMat(matu,matp));

  matp[0]=m_ControlPoints[Index]->Y();
  matp[1]=m_ControlPoints[Index+1]->Y();
  matp[2]=m_ControlPoints[Index+2]->Y();
  if (t >= 1.0)
    matp[3]=m_ControlPoints[Index+2]->Y();
  else
    matp[3]=m_ControlPoints[Index+3]->Y();
  Res.SetY(multMat(matu,matp));

  matp[0]=m_ControlPoints[Index]->Z();
  matp[1]=m_ControlPoints[Index+1]->Z();
  matp[2]=m_ControlPoints[Index+2]->Z();
  if (t >= 1.0)
    matp[3]=m_ControlPoints[Index+2]->Z();
  else
    matp[3]=m_ControlPoints[Index+3]->Z();
  Res.SetZ(multMat(matu,matp));

  return Res;
}

/****************************************************************************/
void CSpline::Draw ()
{
  glDisable(GL_LIGHTING);
  CDrawable::Draw ();
  glEnable(GL_LIGHTING);
}

/****************************************************************************/
float CSpline::multMat(float matu[4], float matp[4])
{
  int i;
  float res;
  float matTemp[4];

  for (i=0; i< 4; i++)
  {
    matTemp[i] = m_Matrix[i][0] * matp[0] \
               + m_Matrix[i][1] * matp[1] \
               + m_Matrix[i][2] * matp[2] \
               + m_Matrix[i][3] * matp[3];
  }

  res = matu[0] * matTemp[0] \
      + matu[1] * matTemp[1] \
      + matu[2] * matTemp[2] \
      + matu[3] * matTemp[3];

  return res;
}

/****************************************************************************/
void CSpline::closeSpline (bool closeSpline, bool continueEnd)
{
  deque<CPoint *>::iterator itPoint;
  CPoint * firstPoint;
  CPoint * secondPoint;
  CPoint * lastPoint;
  //CPoint * secondLastPoint;

  itPoint = m_ControlPoints.begin();
  firstPoint = new CPoint(*itPoint);
  itPoint++;
  secondPoint = new CPoint(*itPoint);

  itPoint = m_ControlPoints.end();
  itPoint--;
  lastPoint = new CPoint(*itPoint);
  //itPoint--;
  //secondLastPoint = new CPoint(*itPoint);

  if (closeSpline && !m_Closed) //enable
  {
    if (m_ReachAllPoints)
      reachAllPoints (false);

    if (continueEnd)
    {
      m_ControlPoints.push_front(lastPoint);
      m_ControlPoints.push_back(firstPoint);
      m_ControlPoints.push_back(secondPoint);
    }
    else
    {
      m_ControlPoints.push_front(firstPoint);
      m_ControlPoints.push_back(firstPoint);
      m_ControlPoints.push_back(firstPoint);
      delete secondPoint;
      delete lastPoint;

    }
    m_Continuous = continueEnd;
    m_Closed = true;
  }

  if (!closeSpline && m_Closed) //disable
  {
    if (m_Continuous)
    {
      delete m_ControlPoints.front ();
      m_ControlPoints.pop_front();
      delete m_ControlPoints.back ();
      m_ControlPoints.pop_back();
      delete m_ControlPoints.back ();
      m_ControlPoints.pop_back();
    }
    else
    {
      delete m_ControlPoints.front ();
      m_ControlPoints.pop_front();
      delete m_ControlPoints.back ();
      m_ControlPoints.pop_back();
      delete m_ControlPoints.back ();
      m_ControlPoints.pop_back();
    }
    m_Continuous = false;
    m_Closed = false;

    delete firstPoint;
    delete secondPoint;
    delete lastPoint;
  }

}

/****************************************************************************/
void CSpline::reachAllPoints (bool reachAll, bool continueEnd)
{
  deque<CPoint *>::iterator itPoint;
  CPoint * firstPoint;
  //CPoint * secondPoint;
  CPoint * lastPoint;
  //CPoint * secondLastPoint;

  itPoint = m_ControlPoints.begin();
  firstPoint = new CPoint(*itPoint);
  //itPoint++;
  //secondPoint = new CPoint(*itPoint);

  itPoint = m_ControlPoints.end();
  itPoint--;
  lastPoint = new CPoint(*itPoint);
  //itPoint--;
  //secondLastPoint = new CPoint(*itPoint);

  if (reachAll && !m_ReachAllPoints) //enable
  {
    if (m_Closed)
      closeSpline (false);

    if (continueEnd)
    {
      m_ControlPoints.push_front (lastPoint);
      m_ControlPoints.push_back (firstPoint);
    }
    else
    {
      m_ControlPoints.push_front(firstPoint);
      m_ControlPoints.push_back(lastPoint);
    }
    m_Continuous = continueEnd;
    m_ReachAllPoints = true;
  }


  if (!reachAll && m_ReachAllPoints) //disable
  {
    if (m_Continuous)
    {
      delete m_ControlPoints.front ();
      m_ControlPoints.pop_front ();
      delete m_ControlPoints.back ();
      m_ControlPoints.pop_back ();
    }
    else
    {
      delete m_ControlPoints.front ();
      m_ControlPoints.pop_front();
      delete m_ControlPoints.back ();
      m_ControlPoints.pop_back();
    }
    m_Continuous = false;
    m_ReachAllPoints = false;

    delete firstPoint;
    delete lastPoint;
  }


}


/********************************* OLD ***************************/

/**************************************************************************** /
int CSpline::getXYIntersectionsWithLine (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, float thirdValue)
{
  CPoint tmpPoint1;
  CPoint tmpPoint2;
  CPoint * tmpIntersection;

  int N = getNbPoints ();
  int NbIntersect = 0;

  if (N >= 4)
  {
    tmpPoint1 = (CPoint) getSplinePoint (0.0);
    for (int Index = 1; Index <= m_Discretization-1; Index++)
    {
      float t = (float)Index/(float)(m_Discretization-1);
      tmpPoint2 = (CPoint) getSplinePoint (t);

      tmpIntersection = Intersect2DLineToSegment (A, B, tmpPoint1, tmpPoint2);
      if (tmpIntersection)
      {
        (*tmpIntersection)[3] = (tmpPoint2.Z() + tmpPoint1.Z()) / 2.0;;
        Intersections.push_back(tmpIntersection);
        NbIntersect++;
      }

      tmpPoint1 = tmpPoint2;
    }
  }

  return NbIntersect;
}
/**/

/**************************************************************************** /
int CSpline::get3DIntersectionsWithSegment (CPoint & A, CPoint & B, deque<CPoint *> & Intersections)
{
  CPoint tmpPoint1;
  CPoint tmpPoint2;
  CPoint * tmpIntersection;

  int N = getNbPoints ();
  int NbIntersect = 0;

  if (N >= 4)
  {
    tmpPoint1 = (CPoint) getSplinePoint (0.0);
    for (int Index = 1; Index <= m_Discretization-1; Index++)
    {
      float t = (float)Index/(float)(m_Discretization-1);
      tmpPoint2 = (CPoint) getSplinePoint (t);

      tmpIntersection = Intersect3DSegments (tmpPoint1, tmpPoint2, A, B);
      if (tmpIntersection)
      {
        Intersections.push_back(tmpIntersection);
        NbIntersect++;
      }

      tmpPoint1 = tmpPoint2;
    }
  }

  return NbIntersect;
}
/**/

/**************************************************************************** /
int CSpline::getXYIntersectionsWithSegment (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, float thirdValue)
{
  CPoint tmpPoint1;
  CPoint tmpPoint2;
  CPoint * tmpIntersection;

  int N = getNbPoints ();
  int NbIntersect = 0;

  if (N >= 4)
  {
    tmpPoint1 = (CPoint) getSplinePoint (0.0);
    for (int Index = 1; Index <= m_Discretization-1; Index++)
    {
      float t = (float)Index/(float)(m_Discretization-1);
      tmpPoint2 = (CPoint) getSplinePoint (t);

      tmpIntersection = Intersect2DSegments (tmpPoint1, tmpPoint2, A, B);
      if (tmpIntersection)
      {
        (*tmpIntersection)[3] = (tmpPoint2.Z() + tmpPoint1.Z()) / 2.0;;
        Intersections.push_back(tmpIntersection);
        NbIntersect++;
      }

      tmpPoint1 = tmpPoint2;
    }
  }

  return NbIntersect;
}
/**/

/**************************************************************************** /
int CSpline::get2DIntersectionsWithSegment (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, int Axis0, int Axis1, int Axis2)
{
  CPoint Point1;
  CPoint Point2;
  CPoint * tmpIntersection;
  CPoint resultIntersection;

  CPoint tmpPoint1;
  CPoint tmpPoint2;
  CPoint tmpA;
  CPoint tmpB;

  int N = getNbPoints ();
  int NbIntersect = 0;

  if (N >= 4)
  {
    Point1 = (CPoint) getSplinePoint (0.0);
    for (int Index = 1; Index <= m_Discretization-1; Index++)
    {
      float t = (float)Index/(float)(m_Discretization-1);
      Point2 = (CPoint) getSplinePoint (t);

      // Convert points to make intersection in XY plan
      tmpPoint1[0] = Point1[Axis0];
      tmpPoint1[1] = Point1[Axis1];
      tmpPoint1[2] = Point1[Axis2];

      tmpPoint2[0] = Point2[Axis0];
      tmpPoint2[1] = Point2[Axis1];
      tmpPoint2[2] = Point2[Axis2];

      tmpA[0] = A[Axis0];
      tmpA[1] = A[Axis1];
      tmpA[2] = A[Axis2];

      tmpB[0] = B[Axis0];
      tmpB[1] = B[Axis1];
      tmpB[2] = B[Axis2];

      // Calculate intersection
      tmpIntersection = Intersect2DSegments (tmpPoint1, tmpPoint2, tmpA, tmpB);
      if (tmpIntersection)
      {
        // Reconvert coordinates of intersaction
        resultIntersection = *tmpIntersection;
        (*tmpIntersection)[Axis0] = resultIntersection[0];
        (*tmpIntersection)[Axis1] = resultIntersection[1];
        (*tmpIntersection)[Axis2] = (tmpPoint1[2] + tmpPoint2[2]) / 2.0;

        Intersections.push_back(tmpIntersection);
        NbIntersect++;
      }

      Point1 = Point2;
    }
  }

  return NbIntersect;
}
/**/

/**************************************************************************** /
int CSpline::getYZIntersectionsWithSegment (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, float thirdValue)
{
  CPoint Point1;
  CPoint Point2;
  CPoint * tmpIntersection;

  CPoint tmpPoint1;
  CPoint tmpPoint2;
  CPoint tmpA;
  CPoint tmpB;

  int N = getNbPoints ();
  int NbIntersect = 0;

  if (N >= 4)
  {
    Point1 = (CPoint) getSplinePoint (0.0);
    for (int Index = 1; Index <= m_Discretization-1; Index++)
    {
      float t = (float)Index/(float)(m_Discretization-1);
      Point2 = (CPoint) getSplinePoint (t);

      tmpPoint1 = Point1;
      tmpPoint1[0] = tmpPoint1[1];
      tmpPoint1[1] = tmpPoint1[2];


      tmpPoint2 = Point2;
      tmpPoint2[0] = tmpPoint2[1];
      tmpPoint2[1] = tmpPoint2[2];

      tmpA = A;
      tmpA[0] = tmpA[1];
      tmpA[1] = tmpA[2];

      tmpB = B;
      tmpB[0] = tmpB[1];
      tmpB[1] = tmpB[2];

      tmpIntersection = Intersect2DSegments (tmpPoint1, tmpPoint2, tmpA, tmpB);
      if (tmpIntersection)
      {
        (*tmpIntersection)[2] = (*tmpIntersection)[1];
        (*tmpIntersection)[1] = (*tmpIntersection)[0];
        (*tmpIntersection)[0] = (Point2.X() + Point1.X()) / 2.0;;
        Intersections.push_back(tmpIntersection);
        NbIntersect++;
      }

      Point1 = Point2;
    }
  }

  return NbIntersect;
}
/**/

/**************************************************************************** /
int CSpline::getYZIntersectionsWithLine (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, float thirdValue)
{
  CPoint Point1;
  CPoint Point2;
  CPoint * tmpIntersection;

  CPoint tmpPoint1;
  CPoint tmpPoint2;
  CPoint tmpA;
  CPoint tmpB;

  int N = getNbPoints ();
  int NbIntersect = 0;

  if (N >= 4)
  {
    Point1 = (CPoint) getSplinePoint (0.0);
    for (int Index = 1; Index <= m_Discretization-1; Index++)
    {
      float t = (float)Index/(float)(m_Discretization-1);
      Point2 = (CPoint) getSplinePoint (t);

      tmpPoint1 = Point1;
      tmpPoint1[0] = tmpPoint1[1];
      tmpPoint1[1] = tmpPoint1[2];


      tmpPoint2 = Point2;
      tmpPoint2[0] = tmpPoint2[1];
      tmpPoint2[1] = tmpPoint2[2];

      tmpA = A;
      tmpA[0] = tmpA[1];
      tmpA[1] = tmpA[2];

      tmpB = B;
      tmpB[0] = tmpB[1];
      tmpB[1] = tmpB[2];

      tmpIntersection = Intersect2DLineToSegment (tmpA, tmpB, tmpPoint1, tmpPoint2);
      if (tmpIntersection)
      {
        (*tmpIntersection)[2] = (*tmpIntersection)[1];
        (*tmpIntersection)[1] = (*tmpIntersection)[0];
        (*tmpIntersection)[0] = (Point2.X() + Point1.X()) / 2.0;;
        Intersections.push_back(tmpIntersection);
        NbIntersect++;
      }

      Point1 = Point2;
    }
  }

  return NbIntersect;
}
/**/
