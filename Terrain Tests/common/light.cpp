/*
 * light.cpp
 *
 *  Created on: May 30, 2010
 *      Author: regis
 */

#include "light.hpp"

/****************************************************************************/
CLight::CLight(float x, float y, float z) : m_Position (x,y,z)
{

}

/****************************************************************************/
CLight::CLight(CPoint & pt) : m_Position (pt)
{

}

/****************************************************************************/
CLight::~CLight()
{

}
