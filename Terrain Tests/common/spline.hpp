/***************************************************************************
 *            spline.h
 *
 *  Sat May 13 15:34:02 2006
 *  Copyright  2006  Regis Martinez
 *  regis.martinez@worldonline.fr
 ****************************************************************************/

#ifndef _SPLINE_H_
#define _SPLINE_H_  

#include <iostream>
#include <deque>
#include "point.hpp"
#include "drawable.hpp"
#include "color.hpp"

using namespace std;


/****************************************************************************/
class CSpline : public CDrawable
{
  protected:
    deque <CPoint *> m_ControlPoints;
    float m_Matrix [4][4];
    int m_Discretization; /* Number of points on each curve chunk (the entire curve is drawn by chunks of 4 control points) */
    CColor m_CurveColor;
    CColor m_PointsColor;

    bool m_DrawPoints;

    bool m_Closed;       // End rejoins the first point ?
    bool m_Continuous;   // End rejoins the first point continuously ?
    bool m_ReachAllPoints; // Ends on the first point, begins on the last point
    bool m_AdaptativeTension; // for catmullrom only

  public:
    /* constructors and destructors */
    CSpline (int m_Discretiation = 10, CColor CurveColor = CColor(0.0, 1.0, 0.0, 1.0), CColor PointsColor = CColor(1.0, 0.0, 1.0, 1.0));
    virtual ~CSpline ();
    
    /* Access methods */
    int getNbPoints ();

    deque<CPoint *> & getControlPoints ();
    int getDiscretization ();
    CColor & getCurveColor ();
    CColor & getPointsColor ();
    void setCurveColor (CColor );
    void setPointsColor (CColor );
    void setDiscretization (int nbSteps);
    void addControlPoints (deque<CPoint *> & ptctl);
    void zeroControlPoints ();

    void drawControlPoints (bool newVal);
    bool drawableCrontrolPoints ();

    void adaptativeTension (bool newVal);
    bool ataptativeTension ();

    /* Members */
    void setBezierMatrix ();
    void setCatmullRomMatrix (float S = 0.5, bool AdaptativeTension = false);
    void setBSplineMatrix ();
    void setHermiteMatrix ();

    void closeSpline (bool closeSpline, bool continueEnd = false);
    void reachAllPoints (bool reachAll, bool continueEnd = false);

    virtual void addPoint (CPoint * newPoint); /* Add a control point */
    virtual void addPoint (CPoint & newPoint); /* Add a control point */
    virtual void addPoint (float x, float y, float z); /* Add a control point */

    CPoint getSplinePoint (float t);
    int get3DIntersections (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, bool lineIntersection = true);
    int get2DIntersections (CPoint & A, CPoint & B, deque<CPoint *> & Intersections, int Axis0 = 0, int Axis1 = 1, int Axis2 = 2, bool lineIntersection = true);

    float multMat(float matu[4], float matp[4]);
    
    /* Function */
    virtual void Build ();
    virtual void Draw ();
    
};



#endif /* _SPLINE_H_ */
