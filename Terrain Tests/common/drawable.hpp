/***************************************************************************
 *            drawable.h
 *
 *  Sat May 13 19:58:21 2010
 *  Copyright  2006  Regis Martinez
 *  regis.martinez@worldonline.fr
 ****************************************************************************/

#ifndef _DRAWABLE_H_
#define _DRAWABLE_H_


/****************************************************************************/
class CDrawable
{
  protected:
    int m_idDrawList;

  public:    CDrawable ();
    virtual ~CDrawable ();


    virtual void Build () = 0;
    virtual void Draw ();
};




#endif /*_DRAWABLE_H_ */
